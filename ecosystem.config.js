module.exports = {
  apps : [{
    name: 'GG',
    script: './start.js',
    args: 'one two',
    instances: 1,
    autorestart: true,
    watch: false,
    instance_var: 'INSTANCE_ID',
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }]
};
