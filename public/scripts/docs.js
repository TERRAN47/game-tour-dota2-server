$('.footer-nav').on('click', function(){
	$('.settings-block').addClass('settings-block-opened')
})

$('.settings-block-content-head i').on('click', function(){
	$('.settings-block').removeClass('settings-block-opened')
})

$('.settings-content-right input').on('input', function(e){
	let token = e.target.value
	localStorage.setItem('token', token)
})


let token = localStorage.getItem('token')
$('.settings-content-right input').val(token)
