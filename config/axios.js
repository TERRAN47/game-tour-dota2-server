import axios from 'axios'

const updateAxiosConfig = async (token) => {

	axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'
	axios.defaults.headers.common['Access-Control-Allow-Methods'] = 'GET,PUT,POST,DELETE'

/*	if(token != null){
		axios.defaults.headers.common['Authorization'] = token
	}else{
		axios.defaults.headers.common['Authorization'] = ''
	}*/

	axios.defaults.baseURL = 'https://api.opendota.com/api/'
}

updateAxiosConfig()

export {
	updateAxiosConfig
}

export default axios