const locationData = [{
	title_country:'Россия',
	country_flag:null,
	cities:[{
		id:12001,
		title:'Москва',
		image:null,
		popular:true
	},{
		id:12002,
		title:'Санкт-Петербург',
		image:null,
		popular:true
	},{
		id:12003,
		title:'Казань',
		image:null,
		popular:false
	},{
		id:12004,
		title:'Екатеринбург',
		image:null,
		popular:false
	},{
		id:12005,
		title:'Нижний Новгород',
		image:null,
		popular:false
	},{
		id:12006,
		title:'Ростов-на-Дону',
		image:null,
		popular:false
	},{
		id:12007,
		title:'Новосибирск',
		image:null,
		popular:true
	}]
}]

export default locationData

