const navList = [{
	title:'Authentication',
	href:'authentication',
	rest:'/api/v1/authentication',
	type:'POST',
	paramsText:'The request body should be a "application/json" encoded object, containing the following items.',
	params:[{
		title:'phone',
		required:true,
		description:'000 000 0000',
		type:'STRING'
	}],
	subNav:null
}]

export default navList

