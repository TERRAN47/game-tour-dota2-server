import {
	ips
} from '../models'

import blackIps from './ips/black.json'


const blacksListIP = () => {

	return [...blackIps]

}

const whitesListIP = [""]

export {
	blacksListIP,
	whitesListIP
}