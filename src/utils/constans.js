const getRang = (score) =>{
  let rang = 1;

  const two = 10;
  const three = 30;
  const four = 90;
  const five = 150;
  const six = 200;
  const seven = 300;
  const eight = 500;
  const nine = 800;

  if(score >= two && score < three){
    return rang = 2
  }else if(score >= three && score < four){
    return rang = 3
  }else if(score >= four && score < five ){
    return rang = 4
  }else if(score >= five && score < six){
    return rang = 5
  }else if(score >= six && score < seven){
    return rang = 6
  }else if(score >= seven && score < eight){
    return rang = 7
  }else if(score >= eight && score < nine){
    return rang = 8
  }else if(score >= nine){
    return rang = 9
  }else{
    return rang
  }
}
export {getRang}