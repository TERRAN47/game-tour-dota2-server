import passport from 'koa-passport'
import steamStrategy from 'passport-steam'
import config from 'config'
import {
  users,
  levels,
  dotaRating,
  csgoRating,
  bans,
  roles,
  balance
} from '../../models'

passport.serializeUser((user, done) => {

  done(null, user)

})

passport.deserializeUser(async (user, done) => {

  done(null, user)

})

passport.use(new steamStrategy.Strategy({
    returnURL: 'https://api.ggameplay.net/v1/auth/steam/return',
    realm: 'https://api.ggameplay.net',
    apiKey: config.STEAM_API_KEY
  }, async (identifier, profile, done) => {

    let findUser = await users.findOne({
      steamId:profile._json.steamid,
    }).deepPopulate('balance')

    if(findUser == null){

      let createLevel = await levels.create({
        rang:1,
        score:'0'
      })

      let createCsgoRating = await csgoRating.create({
        score:0,
        count_games:0
      })

      let createDotaRating = await dotaRating.create({
        score:0,
        count_games:0
      })

      let createRole = await roles.create({
        role:'user'
      })

      let createBan = await bans.create({
        status_ban:false
      })

      let createBalance = await balance.create({})

      let createUser = await users.create({
        firstname:profile._json.personaname,
        steamId:profile._json.steamid,
        steamInfo:profile,
        level:createLevel._doc._id,
        dota_score:createDotaRating._doc._id,
        csgo_score:createCsgoRating._doc._id,
        role:createRole._doc._id,
        ban:createBan._doc._id,
        balance:createBalance._doc._id
      })

      await balance.update({
        _id:createBalance._doc._id
      },{
        user:createUser._doc._id
      })

      await bans.update({
        _id:createBan._doc._id
      },{
        user_id:createUser._doc._id
      })

      await csgoRating.update({
        _id:createCsgoRating._doc._id
      },{
        user_id:createUser._doc._id
      })

      await dotaRating.update({
        _id:createDotaRating._doc._id
      },{
        user_id:createUser._doc._id
      })

      await levels.update({
        _id:createLevel._doc._id
      },{
        user_id:createUser._doc._id
      })

      return done(null, createUser)

    }

    return done(null, findUser)

  }
))

