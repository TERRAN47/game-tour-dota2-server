import mongoose from 'mongoose'
import config from 'config'
 
mongoose.connect(`mongodb://${config.mongodb.host}:${config.mongodb.port}/${config.mongodb.db}`, {
  auth: {
    user: config.mongodb.user,
    password: config.mongodb.password
  },
  autoIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true
})

export default mongoose