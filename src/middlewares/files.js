
import randtoken from 'rand-token'
import gm from 'gm'

export default {

	checkSizeFile: async (ctx, next) => {

		let { photo } = ctx.request.files

		let sizePoster = (photo.size / 1024)

		if(sizePoster <= 5000){

		    ctx.pathFile = photo.path

		    return next()
		}else{

			ctx.status = 400
			return ctx.body = 'Файл слишком велик. Для загрузки используйте файл не больше 5мб'
		}
	},

	saveImage: async (ctx, next) => {

		let { pathFile } = ctx
		let {teamName, abreviation} = ctx.query

		gm.subClass({
			imageMagick: true
		})

		try{

			const generateNameForImage = randtoken.generate(16)
			if(teamName && abreviation){
				gm(pathFile).resize(1000, 850).write(`/var/ggplay/public/uploads/teamLogos/${generateNameForImage}.jpg`, (err) => {
				  	if (err) return console.log('error --->>> ', err)
				  	console.log('Created an image from a Buffer!')
				})

				ctx.imageName = `${generateNameForImage}.jpg`

				return next()
			}else{
				gm(pathFile).resize(1000, 850).write(`/var/ggplay/public/uploads/photos/${generateNameForImage}.jpg`, (err) => {
				  	if (err) return console.log('error --->>> ', err)
				  	console.log('Created an image from a Buffer!')
				})

				ctx.imageName = `${generateNameForImage}.jpg`

				return next()
			}
		}catch(err){
			ctx.status = 500
			return ctx.body = 'Возникла ошибка, попробуйте позже'
		}
	},

	saveImagePrize: async (ctx, next) => {

		let { pathFile } = ctx

		gm.subClass({
			imageMagick: true
		})

		try{

			const generateNameForImage = randtoken.generate(16)

			gm(pathFile).resize(1000, 850).write(`/var/ggplay/public/uploads/prizes/${generateNameForImage}.jpg`, (err) => {
			  	if (err) return console.log('error --->>> ', err)
			  	console.log('Created an image from a Buffer!')
			})

			ctx.imageName = `${generateNameForImage}.jpg`

			return next()

		}catch(err){

			console.log(443, err)

			ctx.status = 500
			return ctx.body = 'Возникла ошибка, попробуйте позже'
		}
	},
}