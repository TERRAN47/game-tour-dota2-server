//middlewares auth
import {
	users,
	roles,
	bans
} from '../models'
import jwt from 'jsonwebtoken'
import config from 'config'

export default {
	authentication: async (ctx, next) => {

		let { authorization } = ctx.request.headers

		if(authorization){

			await jwt.verify(authorization, config.secret, async (err, decoded) => {

			  	if(err){
			  		ctx.status = 401
				  	return ctx.body = {
				  		...err
				  	}
			  	}else{

			  		const findUser = await users.findOne({
			  			_id:decoded.user._id
			  		}).populate('level role dota_score')

			  		if(findUser != null){

				  		ctx.user = findUser._doc
				  		ctx.token = authorization

						return next()

			  		}else{

			  			ctx.status = 400
			  			return ctx.body = 'Ошибка. Пользователь не найден'

			  		}

			  	}
			})

		}else{
			ctx.status = 401
			return ctx.body = {
				error:'Unauthorized'
			}
		}

	},
	checkBot: async (ctx, next) => {
		let { checkBot } = ctx.request.body

		if(checkBot == "myBot"){
			return next()
		}else{
			ctx.status = 401
			return ctx.body = {
				error:'Unauthorized'
			}
		}
	},
	isBan: async (ctx, next) => {

		const { _id } = ctx.state.user

		let searchBan = await bans.findOne({
			user_id:_id
		})

		if(searchBan != null){

			if(searchBan._doc.status_ban){
				ctx.status = 400
				return ctx.body = 'Пользователь заблокирован'
			}

			return next()

		}

		ctx.status = 500
		return ctx.body = 'error'

	},
	isAdmin: async (ctx, next) => {

		let { user } = ctx

		if(user.role && user.role.role == 'ADMINISTRATOR'){

			return next()

		}else{
			ctx.status = 400
			return ctx.body = 'Пользователь не является администратором'
		}

	}
}