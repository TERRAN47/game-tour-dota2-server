
import {

	duelsDota
} from '../models'

export default {

	checkDuelData: async (ctx, next) => {

		let {room_id, team0, team1} = ctx.request.body

    let findRoom = await duelsDota.findOne({_id:room_id}).deepPopulate('bot_id')

		if(findRoom && team0.length > 0 && team1.length > 0){
			ctx.findRoom = findRoom
			return next()
    }else{
      ctx.status = 200
      console.log(222, 'ERROR')
      return ctx.body = 'Не хватает игроков'
    } 
	},
}