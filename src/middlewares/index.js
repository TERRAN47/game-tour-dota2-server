import fieldsMiddleware from './fields'
import authMiddleware from './auth'
import filesMiddleware from './files'
import dotaMiddeleware from './dota'
export {
	fieldsMiddleware,
	authMiddleware,
	filesMiddleware,
	dotaMiddeleware
}