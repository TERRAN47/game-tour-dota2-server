import {
	users,
	tournaments,
	levels,
	csgoRating,
	dotaRating,
	bans,
	balance
} from '../../../models'
import moment from 'moment'
import _ from 'underscore'
import randtoken from 'rand-token'
import config from 'config'
import request from 'request'
import jwt from 'jsonwebtoken'
import fs from 'fs'
import base64Img from 'base64-img'

const algorithm = 'HS512'

export default {
	getUsers: async (ctx, next) => {

		let dataUsers = []

		try{

			let findUsers = await users.find({}, {}, {
			    sort:{
			        createdAt: -1
			    }
			}).deepPopulate('level dota_score csgo_score ban balance')

			console.log(222, findUsers)

			ctx.status = 200
			return ctx.body = findUsers

		}catch(err){

			ctx.status = 400
			return ctx.body = []

		}

	},
	banUser: async (ctx, next) => {

		let { id } = ctx.request.body

		try{

			const banUser = await bans.update({
				user_id:id
			}, {
				status_ban:true
			})

			ctx.status = 200
			return ctx.body = 'Пользователь забанен'

		}catch(err){

			ctx.status = 500
			return ctx.body = 'Ошибка'

		}

	},
	removeUser: async (ctx, next) => {

		let { id } = ctx.request.body

		try{

			await users.findOne({
				_id:id
			}).remove()

			await levels.findOne({
				user_id:id
			}).remove()

			await csgoRating.findOne({
				user_id:id
			}).remove()
			
			await dotaRating.findOne({
				user_id:id
			}).remove()

			ctx.status = 200
			return ctx.body = 'Пользователь удален'

		}catch(err){

			ctx.status = 500
			return ctx.body = 'Ошибка'

		}

	},
	userCheckBalance: async (ctx, next) => {
		let { id } = ctx.request.body

		let getBalanceUserOne = await balance.findOne({
			user:id
		})

		if(getBalanceUserOne){
			return next()
		}else{

			await balance.create({
				user:id
			})

			return next()

		}

	},
	userChangeBalance: async (ctx, next) => {

		let { id, balanceNum } = ctx.request.body

		if(id && balanceNum){

			try{

				let getBalanceUserOne = await balance.findOne({
					user:id
				})

				await balance.update({
					user:id
				}, {
					$inc:{
						balance:Number(balanceNum)
					}
				})

				let getBalanceUser = await balance.findOne({
					user:id
				})

				console.log(22, getBalanceUser, getBalanceUserOne)

				ctx.status = 200
				return ctx.body = {
					data:getBalanceUser
				}

			}catch(e){
				console.log(332, e)
				ctx.status = 500
				return ctx.body = {
					data:'Error'
				}

			}

		}

		ctx.status = 400
		return ctx.body = {
			data:'Передайте все поля'
		}

	}
}