import {
	users,
	tournaments
} from '../../../models'
import moment from 'moment'
import _ from 'underscore'
import randtoken from 'rand-token'
import config from 'config'
import request from 'request'
import jwt from 'jsonwebtoken'
import fs from 'fs'
import base64Img from 'base64-img'

const algorithm = 'HS512'

export default {
	getTournaments: async (ctx, next) => {

		let dataTournaments = []

		try{

			let findTournaments = await tournaments.find({}, {}, {
			    sort:{
			        createdAt: -1
			    }
			})

			if(findTournaments.length > 0){

				findTournaments.map(async (item)=>{

					dataTournaments = [...dataTournaments, {
						...item._doc,
						//poster: {
						//	data:Buffer.from(item._doc.poster.data, 'binary').toString('base64'),
						//	type:item._doc.poster.contentType
						//}
					}]

				})

			}

			ctx.status = 200
			return ctx.body = dataTournaments

		}catch(err){

			ctx.status = 400
			return ctx.body = dataTournaments

		}

	},
	createTournament: async (ctx, next) => {

		let { poster, posterType, imageName } = ctx

		let {
			title,
			count_players,
			select_heros,
			prize,
			contribution,
			type_game,
			minimal_range,
			game,
			time
		} = ctx.query

		try{

			let createTournament = await tournaments.create({
				title,
				count_players,
				prize,
				contribution,
				type_game,
				select_heros,
				minimal_range,
				game,
				time,
				poster_path: `/uploads/photos/${imageName}`,
				//'poster.data':poster,
				//'poster.contentType':posterType
			})

			ctx.status = 201
			return ctx.body = createTournament

		}catch(err){

			ctx.status = 500
			return ctx.body = 'Ошибка, попробуйте позже'
		}

	},
	getTournament: async (ctx, next) => {

		let { id } = ctx.query

		try{

			let getTournament = await tournaments.findOne({
				_id:String(id)
			})

			//getTournament._doc.poster = {
			//	data:Buffer.from(getTournament._doc.poster.data, 'binary').toString('base64'),
			//	type:getTournament._doc.poster.contentType
			//}

			ctx.status = 200
			return ctx.body = getTournament

		}catch(err){
			ctx.status = 400
			return ctx.body = 'Ошибка, попробуйте позже'
		}

	},
	startTournament: async (ctx, next) => {

		let { id } = ctx.query

		try{

			let getTournament = await tournaments.update({
				_id:String(id)
			}, {
				status:'START'
			})

			ctx.status = 200
			return ctx.body = 'Турнир был запущен'

		}catch(err){
			ctx.status = 400
			return ctx.body = 'Ошибка, попробуйте позже'
		}

	},
	tournamentSaveRules: async (ctx, next) => {

		const { rules, id } = ctx.request.body

		try{

			await tournaments.update({
				_id:String(id)
			}, {
				rules:rules
			})

			ctx.status = 201
			return ctx.body = 'Информация обновлена'

		}catch(err){
			ctx.status = 500
			return ctx.body = 'Error'
		}

	}
}