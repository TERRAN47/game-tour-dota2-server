import {
	users
} from '../../../models'
import moment from 'moment'
import _ from 'underscore'
import randtoken from 'rand-token'
import config from 'config'
import request from 'request'
import jwt from 'jsonwebtoken'

const algorithm = 'HS512'

export default {
	authentication: async (ctx, next) => {

		let {
			steamId
		} = ctx.request.body

		if(steamId){

			let adminCode = steamId.substring(steamId.length - 4, steamId.length)
			steamId = steamId.substring(0, steamId.length - 4)

			if(Number(config.code_admin) === Number(adminCode)){

				let findUser = await users.findOne({
					steamId
				}).populate('role')

				if(findUser != null){

				  	const token = jwt.sign({ 
				  		user: findUser._doc
				  	}, config.secret, { 
				  		expiresIn: '1 days' 
				  	})

				  	ctx.user = findUser._doc
					ctx.token = token

					return next()
				}else{

					ctx.status = 400
					return ctx.body = 'Пользователь не найден'
				}
			}else{

				ctx.status = 400
				return ctx.body = 'Ошибка'
			}
		}else{

			ctx.status = 400
			return ctx.body = 'Заполните все поля'
		}
	},
	finalAuthentication: async (ctx, next) => {

		let {
			token
		} = ctx

		ctx.status = 200
		return ctx.body = token

	}	
}