import {
	prizes
} from '../../../models'
import moment from 'moment'
import _ from 'underscore'
import randtoken from 'rand-token'
import config from 'config'
import request from 'request'
import jwt from 'jsonwebtoken'
import fs from 'fs'
import base64Img from 'base64-img'

const algorithm = 'HS512'

export default {
	getPrizes: async (ctx, next) => {

		try{

			let getPrizes = await prizes.find({}).deepPopulate('admin')

			ctx.status = 200
			return ctx.body = getPrizes

		}catch(err){

			ctx.status = 500
			return ctx.body = 'Error'

		}

	},
	removePrize: async (ctx, next) => {

		let { id } = ctx.request.body

		try{

			let removePrize = await prizes.find({
				_id:id
			}).remove()

			ctx.status = 200
			return ctx.body = removePrize

		}catch(err){

			ctx.status = 500
			return ctx.body = 'Error'

		}

	},
	create: async (ctx, next) => {

		let { imageName } = ctx

		let {
			title,
			price,
		} = ctx.query

		try{

			let createPrize = await prizes.create({
				title,
				price,
				poster: `/uploads/prizes/${imageName}`
			})

			ctx.status = 201
			return ctx.body = createPrize

		}catch(err){

			ctx.status = 500
			return ctx.body = 'Ошибка, попробуйте позже'
		}

	}
}