import {
	transactions,
	transfers
} from '../../../models'
import moment from 'moment'
import _ from 'underscore'
import randtoken from 'rand-token'
import config from 'config'
import request from 'request'
import jwt from 'jsonwebtoken'
import fs from 'fs'
import base64Img from 'base64-img'

const algorithm = 'HS512'

export default {
	getTransactions: async (ctx, next) => {

		try{

			let getTransaction = await transactions.find({}).deepPopulate('user')

			ctx.status = 200
			return ctx.body = getTransaction

		}catch(err){

			ctx.status = 500
			return ctx.body = 'Error'

		}

	},
	createTransaction: async (ctx, next) => {

		const generateNum = randtoken.generate(9, '0123456789')
		const generateSum = randtoken.generate(4, '123456789')

		try{

			let getTransaction = await transactions.create({
				user:'5d908ede0aa45d07f77553d8',
				transaction_code:generateNum,
				amount:String(generateSum)
			})

			ctx.status = 200
			return ctx.body = 'created'

		}catch(err){

			console.log(22, err)

			ctx.status = 500
			return ctx.body = 'Error'

		}	

	},

	getTransfers: async (ctx, next) => {

		let getTransfers = await transfers.find({}).sort({status: 1}).deepPopulate('user_id')

		ctx.status = 200
		return ctx.body = getTransfers

	},

	removeTransfer: async (ctx, next) => {

		let { id } = ctx.request.body

		await transfers.update({
			_id:id
		}, {
			status:true
		})

		ctx.status = 200
		return ctx.body = 'Перевод успешно обновлен'

	}
}