import {
	boosts
} from '../../../models'
import moment from 'moment'
import _ from 'underscore'
import randtoken from 'rand-token'
import config from 'config'
import request from 'request'
import jwt from 'jsonwebtoken'
import fs from 'fs'
import base64Img from 'base64-img'

const algorithm = 'HS512'

export default {
	GET: async (ctx, next) => {

		try{

			let data = await boosts.find({})

			ctx.status = 200
			return ctx.body = data

		}catch(err){
			ctx.status = 500
			return ctx.body = 'Error'
		}

	}
}