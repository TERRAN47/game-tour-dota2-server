import {
	users,
	teams,
	notifyTeams,
	balance
} from '../../../models'
import moment from 'moment'
import _ from 'underscore'

export default {
	getNitifications: async (ctx, next) => {

		let { user } = ctx

		try{
			const findTeamsNotify = await notifyTeams.find({user:user._id}).deepPopulate("team, team.leader_id")

			
			ctx.status = 200
			return ctx.body = {
				notifyTeams:findTeamsNotify
			}
		}catch(err){
			ctx.status = 400
			return "ERROR"
		}
	},
	aproovTeam: async (ctx) => {
		try{
			let { user } = ctx
			const {
				team_id
			} = ctx.params

			const findTeam = await teams.findOne({_id:team_id})
			
			if(findTeam){
				if(user.team == undefined || user.team == null || `${user.team}` == `${user._id}`){
					findTeam.reservTeam.push(user._id)
				
					let team = await teams.updateOne({_id:team_id}, {reservTeam: findTeam.reservTeam})
					await users.updateOne({_id:user._id}, {
						team: team_id
					})

					await notifyTeams.remove({user:user._id, team:team_id})
					
					ctx.status = 200;
					return ctx.body = "OK"
				}
			}else{
				return ctx.status = 400
			}
		}catch(err){
			ctx.status = 400
			return "ERROR"
		}		
	},
	notAproovTeam: async (ctx, next) => {

		try{
			let { user } = ctx
			const {
				team_id
			} = ctx.params
			if(team_id){
				await notifyTeams.remove({user:user._id, team:team_id})
				ctx.status = 200
				return ctx.body = "OK"
			}else{
				ctx.status = 400
				return "ERROR"
			}
		}catch(err){
			ctx.status = 400
			return "ERROR"
		}
	},
	sendTeamNitify: async (ctx, next) => {
		// const {
		// 	team_id
		// } = ctx.params
		let {user_id_invite, team_id} = ctx.request.body
		//let { user } = ctx

		try{
			if(user_id_invite && team_id){
				const findInviteUSer = await notifyTeams.findOne({user:user_id_invite, team:team_id})
				let dataInfo = "Приглашение оправленно!"
				if(findInviteUSer){
					dataInfo = "Игрок еще не принял решение"
				}else{
					await notifyTeams.create({user:user_id_invite, team:team_id})
				}
				ctx.status = 200
				return ctx.body = dataInfo
			}else{
				ctx.status = 400
				return "ERROR"
			}
		}catch(err){
			ctx.status = 400
			return "ERROR"
		}
	},
}