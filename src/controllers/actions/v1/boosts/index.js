import {
	users,
	resultGames,
	teams,
	boosts
} from '../../../../models'
import _ from 'underscore'
import fs from 'fs'
import im from 'imagemagick'
import randtoken from 'rand-token'

export default {
	createBoost: async (ctx, next) => {

		let {
			email = '',
			loginSteam = '',
			passwordSteam = '',
			vk = '',
			phone = '',
			wishes = '',
			mmr = '0',
			type = 'BOOST_RATING',
			boostParams = {
				boost_party_mmr:false,
				to_run_faster:false,
				dota_play_on_my_heroes:false,
				do_not_play_at_certain_times:false,
			},
			skill_point = '',
			price = '',
			games = '',
			previous_level = '',
			previous_rang = '',
			previous_point_mmr = '',
			real_point_mmr = '',
			days = '0',
			training = '0'

		} = ctx.request.body

		let {
			boost_party_mmr,
			to_run_faster,
			dota_play_on_my_heroes,
			do_not_play_at_certain_times,
		} = boostParams

		let { user } = ctx

		try{

			let createBoostApplication = await boosts.create({
				user_id:user._id,
				type_boost:type,
				skill_point:skill_point,
				price:price,
				vk:vk,
				email:email,
				games:games,
				previous_level:previous_level,
				previous_rang:previous_rang,
				previous_point_mmr:previous_point_mmr,
				login_steam:loginSteam,
				password_steam:passwordSteam,
				phone:phone,
				real_point_mmr:real_point_mmr,
				boost_party_mmr:boost_party_mmr,
				to_run_faster:to_run_faster,
				dota_play_on_my_heroes:dota_play_on_my_heroes,
				do_not_play_at_certain_times:do_not_play_at_certain_times,
				wishes:wishes,
				mmr:mmr,
				days:days,
				training:training,
			})

			ctx.status = 200
			return ctx.body = 'Создано'

		}catch(err){

			ctx.status = 500
			return ctx.body = 'ошибка'

		}

	}
}