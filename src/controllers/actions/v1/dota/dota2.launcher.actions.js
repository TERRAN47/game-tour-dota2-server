import {
  users,
  duelsDota,
  levels,
  dotaHeros,
  balance,
  resultDotaGames,
  bots,
  dotaRating
} from '../../../../models'

import moment from 'moment'
import _ from 'underscore'
import randtoken from 'rand-token'
import Steam from 'steam'
import dota2 from 'dota2'
import Long from "long"
import axios from '../../../../../config/axios'
import cron from 'node-cron';
import actionsLaunch from './dota2.launcher.actions'
import {getRang} from "../../../../utils/constans"
import tournamActions from "./tournament.actions"
const {checkComplitGames, restartAllBots, restartTournaments, startTournament} = tournamActions
let steamClient = new Steam.SteamClient()
let steamUser = new Steam.SteamUser(steamClient);
let Dota2 = new dota2.Dota2Client(steamClient, true, false);
//console.log(8888888, dota2.schema.DOTA_GameMode)
steamClient.on('servers', (data)=>{
  console.log(777777, data);
});

steamClient.on('error', (data)=>{
  console.log(666999, data);
});

// steamClient.on('connected', ()=>{

//   console.log(333,'connected steam')

//   steamUser.logOn({
//     account_name: "Terran4789",
//     password: "Arhangel6",
//     // auth_code:'HK6Y3'
//   })
// });
//steamClient.connect();

steamClient.on('loggedOff', (data)=>{

  steamClient.connect();
});

/*Если в элементе находится число — то выбранная часть даты/времени будет проверяться на совпадение с этим числом. Например, 
«* 57 * * 5 2″ будет срабатывать в каждый вторник мая в каждую 57-ю минуту — в 00:57:00, в 01:57:00 и так далее. «* 0 * * * *» 
будет срабатывать каждое начало часа.Если в элементе находится выражение — список или диапазон, например «* 28-31 * * * 5-6″ 
сработает каждую пятницу и субботу с 28 по 31 минуту. Также можно указывать более осмысленные расписания, например */

// «* */5 * * * *» — каждая пятая минута, задание будет выполняться раз в пять минут. Ну а «* */5 9-18 * * * *» —
//раз в пять минут, но если время — с 9 часов утра по 6 вечера. 
// Можно поставить туда echo «не отвлекайся на консоль, работай!»
 
cron.schedule('0 0 0 * * *', () => {
  const time = moment().format();
  console.log('каждый день!', time);
  restartTournaments()
  restartAllBots()
});

cron.schedule('0 0 */1 * * *', () => {
  const time = moment().format("HH:mm");
  console.log('каждый час!', time);
  startTournament(time)
 
});

cron.schedule('*/9 * * * *', () => {
 
	checkComplitGames({})
  const time = moment().format("HH:mm");
  console.log(time, 'running a task every 9 minutes');
});

//cron.schedule('*/5 * * * *', () => {
/*
  const time = moment().format("HH:mm");
  console.log(time, 'running a task every 5 minutes');  
  startTournament(time)
});*/
Dota2.on('ready', ()=>{

	console.log(1213, "ready")
});

export default {
  createOneVsOneLobby: async (ctx) => {
    let {room_id, team0, findFreeBotId, findRoom, leagueid, restartLoby, user_id, team1} = ctx.request.body

    let timeOutLobby
    let updateStatus = true
    //findRoom = findRoom._doc
    let reqClient = 'ok';
    try{
    
	    console.log(2222, steamClient.connected)
	    
      if(String(findRoom.hoster_id) == user_id && findFreeBotId && leagueid){

        let randomTitle = `GGamePlay.net ${Math.floor(Math.random() * 100000)}`;
        let randomPass = Math.floor(Math.random() * 100000).toString();

       // steamClient.on('logOnResponse', async (data)=>{

       		//if(data.eresult == 1){
            let properties = {
              "game_name": randomTitle,
              "server_region": dota2.ServerRegion.EUROPE,
              "game_mode": dota2.schema.DOTA_GameMode.DOTA_GAMEMODE_1V1MID,
              "series_type": dota2.SeriesType.BEST_OF_THREE,
              "game_version": 1,
              "allow_cheats": false,
              "fill_with_bots": false,
              "allow_spectating": true,
              "pass_key": randomPass,
              "radiant_series_wins":0,
              "dire_series_wins": 0,
              "allchat": true,
              "dota_tv_delay":0,
              "pause_setting":0,
              "leagueid": +(leagueid) //11058 //11457 
            }

            let lobbyChannel

            let lobbyTime = 4;
            const deleteLobby = () => {
	            if(lobbyTime > 0){
                timeOutLobby = setTimeout(()=>{
                  Dota2.sendMessage(`Waiting time for all players ${lobbyTime}m...`, lobbyChannel, 3);


                  lobbyTime -= 1;
                  deleteLobby();                  
                }, 60000)
	            }else{
              	clearTimeout(timeOutLobby);

                Dota2.destroyLobby(async(err, data)=>{
                	await bots.updateOne({_id:findFreeBotId}, {status:true})
                	Dota2.leaveChat(lobbyChannel);
                  if (err) {
                    console.log(err + ' - ' + JSON.stringify(data));
                  } else {
                  	console.log(33333333, data)
                  	Dota2.sendMessage(`Missing players will be defeated`, lobbyChannel, 3);
                  }
                });
	            }
            }
				
           // Dota2.on('ready', ()=>{
         	 		
	          	const createNewLoby = async () =>{
				        if(team0[0].account_id){ }else{
				          await users.updateOne({_id:team0[0]._id}, {
				            account_id: Dota2.ToAccountID(team0[0].steamId)
				          })
				        }
				        
				        if(team1[0].account_id){ }else{
				          await users.updateOne({_id:team1[0]._id}, {
				            account_id: Dota2.ToAccountID(team1[0].steamId)
				          })
				        }
	              Dota2.createPracticeLobby(properties, async (err, data) => {
	                if(err){
	                  console.log('error ---> ' + err + ' - ' + data)
	                 	setTimeout(async ()=>{
	                 		createNewLoby()
	                 	}, 2000)
	                }else{
                    if(data && data.eresult == 1){
                      
                      reqClient = 'ЛОББИ УСПЕШНО СОЗДАННО!'
                      console.log(111, reqClient)
                      await duelsDota.updateOne({_id:room_id}, {lobby_login:{randomTitle, randomPass}});

                      Dota2.inviteToLobby(team0[0].steamInfo.id);
                      Dota2.inviteToLobby(team1[0].steamInfo.id);

                      await duelsDota.updateOne({_id:room_id}, {bot_id: findFreeBotId, $inc: { count_create_loby: +1 }})

                      let lobbyID = null
                      Dota2.on("practiceLobbyUpdate", async (lobby) => { 
                        if(lobbyID == null){
                          lobbyID = new Long(lobby.lobby_id.low, lobby.lobby_id.high, lobby.lobby_id.unsigned).toString();
                          lobbyChannel = "Lobby_"+lobbyID;
                          Dota2.joinChat(lobbyChannel, dota2.schema.DOTAChatChannelType_t.DOTAChannelType_Lobby);                                 
                        }
                       // console.log(123456, lobby)
                        if(properties.game_mode == 21){
                        if(lobby.members && lobby.members.length > 1){
                    
                          let checkUserTeam0 = _.findWhere(lobby.members, {
                            name:team0[0].steamInfo._json.personaname
                          })

                          let checkUserTeam1 = _.findWhere(lobby.members, {
                            name:team1[0].steamInfo._json.personaname
                          })

                          if(checkUserTeam0 || checkUserTeam1){
                            let findMember0 = _.findWhere(lobby.members, {
                              team:0
                            })

                            let findMember1 = _.findWhere(lobby.members, {
                              team:1
                            })

                            if(findMember0){
                              if(team0.length == 1){
                                let accountIdT0 = new Long(findMember0.id.low, findMember0.id.high, findMember0.id.unsigned).toString();
                                if(team0[0].steamId == accountIdT0 && team0.length < 2){
                                  // OK
                                }else{
                                  Dota2.practiceLobbyKickFromTeam(Dota2.ToAccountID(accountIdT0))
                                }
                              }else{
                                let accountIdT0 = new Long(findMember0.id.low, findMember0.id.high, findMember0.id.unsigned).toString();
                                Dota2.practiceLobbyKickFromTeam(Dota2.ToAccountID(accountIdT0))
                              }
                            }
                            
                            if(findMember1){
                              if(team1.length == 1){
                                let accountIdT1 = new Long(findMember1.id.low, findMember1.id.high, findMember1.id.unsigned).toString();
                                if(team1[0].steamId == accountIdT1 && team1.length < 2){
                                  // OK
                                }else{
                                  Dota2.practiceLobbyKickFromTeam(Dota2.ToAccountID(accountIdT1))
                                }
                              }else{
                                let accountIdT1 = new Long(findMember1.id.low, findMember1.id.high, findMember1.id.unsigned).toString();
                                Dota2.practiceLobbyKickFromTeam(Dota2.ToAccountID(accountIdT1))
                              }
                            }

                            if(lobby.members.length == 3 && findMember0 && findMember1){
                              if(lobby.match_id){
                                let matchID = new Long(lobby.match_id.low, lobby.match_id.high, lobby.match_id.unsigned).toString();
                                
                                let updateRoom = null
                                //if(updateStatus){
                                
                                let resultUpdate = await duelsDota.updateOne({'_id':room_id}, {'match_id':matchID, 'lobby_id':lobbyID, 'lobby_info':lobby});
                                //console.log(1233, resultUpdate)
                                  //updateStatus = false            
                                //}
                                //console.log(77, matchID)
                              }else{
                                if(updateStatus){
                                  updateStatus = false
                                  let timeStartItem = 5
                                  let intervalStart = setInterval(async ()=>{                                   
                                    if(timeStartItem < 1){
                                      clearInterval(intervalStart)
                                      await duelsDota.updateOne({'_id':room_id}, {'lobby_info':lobby});

                                      // Dota2.launchPracticeLobby();
                                      // Dota2.leavePracticeLobby();
                                      // Dota2.abandonCurrentGame();
                                      Dota2.launchPracticeLobby(()=>{
                                        Dota2.leavePracticeLobby((err, data)=>{
                                          if (!err) {
                                            console.log('LEAVE LOBBY', data)
                                            Dota2.abandonCurrentGame();
                                          } else {
                                            console.log('ERROR', err)
                                          }
                                        });
                                      }) 

                                      await bots.updateOne({"_id":findFreeBotId}, {"status":true});

                                      Dota2.leaveChat(lobbyChannel);

                                    }else{
                                      Dota2.sendMessage(`start in ${timeStartItem} seconds...`, lobbyChannel, 3);
                                      timeStartItem--
                                    }
                                  }, 1000);
                                    intervalStart();
                                }
                              }
                            }
                          }else{
                            let findMember0 = _.findWhere(lobby.members, {
                              team:0
                            })

                            let findMember1 = _.findWhere(lobby.members, {
                              team:1
                            })

                            if(findMember0){
                              let accountIdTeam0 = new Long(findMember0.id.low, findMember0.id.high, findMember0.id.unsigned).toString();


                              if(team0.steamInfo._json.personaname != findMember0.name){
                                Dota2.practiceLobbyKickFromTeam(Dota2.ToAccountID(accountIdTeam0))
                              }
                            }
                            if(findMember1){
                              let accountIdTeam1 = new Long(findMember1.id.low, findMember1.id.high, findMember1.id.unsigned).toString();
                              if(team1.steamInfo._json.personaname != findMember1.name){
                                Dota2.practiceLobbyKickFromTeam(Dota2.ToAccountID(accountIdTeam1))
                              }
                            }
                          }
                        }                             
                        }    
                      })
                    }else if(data.eresult == 2){
                      reqClient = 'БОТ СОСТОИТ В ДРУГОМ ЛОББИ'
                      Dota2.destroyLobby()
                      setTimeout(()=>{
                        createNewLoby()
                      }, 2000)
                      console.log(44434, reqClient)
                      return reqClient
                    }else{
                      reqClient = "ОШИБКА"
                      console.log(333, 'ДРУГАЯ ПРОБЛЕММА', data)
                      return reqClient
                    }
                  }
	              })
	            }

	           	if(steamClient.connected){
               
                Dota2.launch();
                deleteLobby()
	            	createNewLoby()
				    	}else{
                steamClient = new Steam.SteamClient();
                steamUser = new Steam.SteamUser(steamClient);
                Dota2 = new dota2.Dota2Client(steamClient, true, false);

                steamClient.connect();

                setTimeout(()=>{
       
                  //console.log(6666666,steamUser,  "logOn")
                  steamUser.logOn({
                    account_name: "Terran4789",
                    password: "Arhangel6"
                    // auth_code:'HK6Y3'
                  })
                  
                  setTimeout(()=>{
                    actionsLaunch.createOneVsOneLobby(ctx)
                    //createNewLoby()
                  }, 3000)

                }, 5000)

				    	}
            //})
        //   }else{
        //     console.log(9898978, data)
        //   }
          //})
	      ctx.status = 200
	      return ctx.body = "ok"
      }else{
        ctx.status = 500
        await bots.updateOne({_id:findFreeBotId}, {status:true})
        console.log(111, 'ERROR')
        return ctx.body = {
          err:".!."
        }
      }
    }catch(err){
      await bots.updateOne({_id:findFreeBotId}, {status:true})
      ctx.status = 500
      console.log(333, err)
      return ctx.body = "ERROR"
    }
  },


  createTwoVsTwoLobby: async (ctx) => {
    let {room_id, team0, findFreeBotId, findRoom, leagueid, restartLoby, user_id, team1} = ctx.request.body

    let timeOutLobby
    let updateStatus = true
    //findRoom = findRoom._doc
    let reqClient = 'ok';
    try{
    
      if(String(findRoom.hoster_id) == user_id && findFreeBotId && leagueid){

      
        let randomTitle = `GGamePlay.com ${Math.floor(Math.random() * 100000)}`;
        let randomPass = Math.floor(Math.random() * 100000).toString();

        if(team0[0].account_id){ }else{
          await users.updateOne({_id:team0[0]._id}, {
            account_id: Dota2.ToAccountID(team0[0].steamId)
          })
        }
        
        if(team1[0].account_id){ }else{
          await users.updateOne({_id:team1[0]._id}, {
            account_id: Dota2.ToAccountID(team1[0].steamId)
          })
        }
       // steamClient.on('logOnResponse', async (data)=>{

          //if(data.eresult == 1){
            let properties = {
              "game_name": randomTitle,
              "server_region": dota2.ServerRegion.EUROPE,
              "game_mode": dota2.schema.DOTA_GameMode.DOTA_GAMEMODE_1V1MID,
              "series_type": dota2.SeriesType.BEST_OF_THREE,
              "game_version": 1,
              "allow_cheats": false,
              "fill_with_bots": false,
              "allow_spectating": true,
              "pass_key": randomPass,
              "radiant_series_wins":0,
              "dire_series_wins": 0,
              "allchat": true,
              "dota_tv_delay":0,
              "pause_setting":0,
              "leagueid": +(leagueid) //11058 //11457 
            }

            let lobbyChannel

            let lobbyTime = 4;
            const deleteLobby = () => {
              if(lobbyTime > 0){
                timeOutLobby = setTimeout(()=>{
                  Dota2.sendMessage(`Waiting time for all players ${lobbyTime}m...`, lobbyChannel, 3);
                  lobbyTime -= 1 
                  deleteLobby()                     
                }, 60000)
              }else{
                clearTimeout(timeOutLobby);
                Dota2.destroyLobby(async(err, data)=>{
                  await bots.updateOne({_id:findFreeBotId}, {status:true})
                  Dota2.leaveChat(lobbyChannel);
                  if (err) {
                    console.log(err + ' - ' + JSON.stringify(data));
                  } else {
                    
                    Dota2.sendMessage(`Missing players will be defeated`, lobbyChannel, 3);
                  }
                });
              }
            }
            Dota2.launch();
            deleteLobby()

            
            //Dota2.on('ready', ()=>{
              
              const createNewLoby = () =>{ 
                Dota2.createPracticeLobby(properties, async (err, data) => {
                  if(err){
                    console.log('error ---> ' + err + ' - ' + data)
                    setTimeout(()=>{
                      createNewLoby()
                    }, 2000)
                  }
                  
                  if(data.eresult == 1){
                    reqClient = 'ЛОББИ УСПЕШНО СОЗДАННО!'
                    console.log(111, reqClient)
                    await duelsDota.updateOne({_id:room_id}, {lobby_login:{randomTitle, randomPass}});

                    Dota2.inviteToLobby(team0[0].steamInfo.id);
                    Dota2.inviteToLobby(team1[0].steamInfo.id);

                    await duelsDota.updateOne({_id:room_id}, {bot_id: findFreeBotId, $inc: { count_create_loby: +1 }})
                    await bots.updateOne({_id:findFreeBotId}, {room_id, status:false})

                    Dota2.on("practiceLobbyUpdate", async (lobby) => { 
                     
                      let lobbyID = new Long(lobby.lobby_id.low, lobby.lobby_id.high, lobby.lobby_id.unsigned).toString();
                      lobbyChannel = "Lobby_"+lobbyID;

    
                      Dota2.joinChat(lobbyChannel, dota2.schema.DOTAChatChannelType_t.DOTAChannelType_Lobby);                                 
                      
                     // console.log(123456, lobby)
                      if(properties.game_mode == 21){
                      if(lobby.members && lobby.members.length > 1){
                  
                        let checkUser1Team0 = _.findWhere(lobby.members, {
                          name:team0[0].steamInfo._json.personaname
                        })
                        let checkUser2Team0 = _.findWhere(lobby.members, {
                          name:team0[1].steamInfo._json.personaname
                        })

                        let checkUser1Team1 = _.findWhere(lobby.members, {
                          name:team1[0].steamInfo._json.personaname
                        })
                        let checkUser2Team1 = _.findWhere(lobby.members, {
                          name:team1[1].steamInfo._json.personaname
                        })

                        if(checkUser1Team0 || checkUser2Team0 || checkUser1Team1){
                          let findMember0 = _.findWhere(lobby.members, {
                            team:0
                          })

                          let findMember1 = _.findWhere(lobby.members, {
                            team:1
                          })

                          if(findMember0){


                            if(team0.length == 1){
                              let accountIdT0 = new Long(findMember0.id.low, findMember0.id.high, findMember0.id.unsigned).toString();
                              if(team0[0].steamInfo._json.personaname == findMember0.name){
                                // OK
                              }else{
                                Dota2.practiceLobbyKickFromTeam(Dota2.ToAccountID(accountIdT0))
                              }
                            }else{
                              let accountIdT0 = new Long(findMember0.id.low, findMember0.id.high, findMember0.id.unsigned).toString();
                              Dota2.practiceLobbyKickFromTeam(Dota2.ToAccountID(accountIdT0))
                            }
                          }
                          
                          if(findMember1){
                            if(team1.length == 1){
                              let accountIdT1 = new Long(findMember1.id.low, findMember1.id.high, findMember1.id.unsigned).toString();
                              if(team1[0].steamInfo._json.personaname == findMember1.name && team1.length == 1){
                                // OK
                              }else{
                                Dota2.practiceLobbyKickFromTeam(Dota2.ToAccountID(accountIdT1))
                              }
                            }else{
                              let accountIdT1 = new Long(findMember1.id.low, findMember1.id.high, findMember1.id.unsigned).toString();
                              Dota2.practiceLobbyKickFromTeam(Dota2.ToAccountID(accountIdT1))
                            }
                          }

                          if(lobby.members.length == 3 && findMember0 && findMember1){
                            if(lobby.match_id){
                              let matchID = new Long(lobby.match_id.low, lobby.match_id.high, lobby.match_id.unsigned).toString();
                              
                              let updateRoom = null
                              //if(updateStatus){
                              
                              let resultUpdate = await duelsDota.updateOne({'_id':room_id}, {'match_id':matchID, 'lobby_id':lobbyID, 'lobby_info':lobby});
                              //console.log(1233, resultUpdate)
                                //updateStatus = false            
                              //}
                              //console.log(77, matchID)
                            }else{
                              if(updateStatus){
                                updateStatus = false
                                let timeStartItem = 5
                                let intervalStart = setInterval(async ()=>{                                   
                                  if(timeStartItem < 1){
                                    clearInterval(intervalStart)
                                    await duelsDota.updateOne({'_id':room_id}, {'lobby_info':lobby});

                                    // Dota2.launchPracticeLobby();
                                    // Dota2.leavePracticeLobby();
                                    // Dota2.abandonCurrentGame();
                                    Dota2.launchPracticeLobby(()=>{
                                      Dota2.leavePracticeLobby((err, data)=>{
                                        if (!err) {
                                          console.log('LEAVE LOBBY', data)
                                          Dota2.abandonCurrentGame();
                                        } else {
                                          console.log('ERROR', err)
                                        }
                                      });
                                    }) 

                                    await bots.updateOne({"_id":findFreeBotId}, {"status":true});

                                    Dota2.leaveChat(lobbyChannel);

                                  }else{
                                    Dota2.sendMessage(`start in ${timeStartItem} seconds...`, lobbyChannel, 3);
                                    timeStartItem--
                                  }
                                }, 1000);
                                  intervalStart();
                              }
                            }
                          }
                        }else{
                          let findMember0 = _.findWhere(lobby.members, {
                            team:0
                          })

                          let findMember1 = _.findWhere(lobby.members, {
                            team:1
                          })

                          if(findMember0){
                            let accountIdTeam0 = new Long(findMember0.id.low, findMember0.id.high, findMember0.id.unsigned).toString();


                            if(findMember0[0].steamInfo._json.personaname != findMember0.name){
                              Dota2.practiceLobbyKickFromTeam(Dota2.ToAccountID(accountIdTeam0))
                            }
                          }
                          if(findMember1){
                            let accountIdTeam1 = new Long(findMember1.id.low, findMember1.id.high, findMember1.id.unsigned).toString();
                            if(findMember1[0].steamInfo._json.personaname != findMember1.name){
                              Dota2.practiceLobbyKickFromTeam(Dota2.ToAccountID(accountIdTeam1))
                            }
                          }
                        }
                      }                             
                      }    
                    })
                  }else if(data.eresult == 2){
                    reqClient = 'БОТ СОСТОИТ В ДРУГОМ ЛОББИ'
                    Dota2.destroyLobby()
                    setTimeout(()=>{
                      createNewLoby()
                    }, 2000)
                    console.log(44434, reqClient)
                    return reqClient
                  }else{
                    reqClient = "ОШИБКА"
                    console.log(333, 'ДРУГАЯ ПРОБЛЕММА', data)
                    return reqClient
                  }
                })
              }

              createNewLoby()
            //})
        //   }else{
        //     console.log(9898978, data)
        //   }
        // })
        ctx.status = 200
        return ctx.body = "ok"
      }else{
        ctx.status = 500
        console.log(111, 'ERROR')
        return ctx.body = {
          err:".!."
        }
      }
    }catch(err){
      ctx.status = 500
      console.log(333, err)
      return ctx.body = "ERROR"
      console.log('ERROR', err)
    }
  },

  
  resultMatch: async (ctx)=>{
    const {
    room_id
    } = ctx.params

    try{
      let findRoom = await duelsDota.findOne({_id:room_id}).deepPopulate('team0 team1')
      if(findRoom){
        const match_id = findRoom.match_id;

        if(match_id != null ){
          const {data} = await axios.get(`/matches/${match_id}`)
          const result_match = data;
          const lobby = findRoom.lobby_info;
          const team0 = findRoom.team0;
          const team1 = findRoom.team1;

          if(data.error == undefined){
            if(findRoom.type_duel == "ONE_BY_ONE" && result_match.match_id){

              let checkUserTeam0 = _.findWhere(lobby.members, {
                name:team0[0].steamInfo._json.personaname
              })

              let checkUserTeam1 = _.findWhere(lobby.members, {
                name:team1[0].steamInfo._json.personaname
              })

              let findMember0 = _.findWhere(lobby.members, {
                team:0
              })

              let findMember1 = _.findWhere(lobby.members, {
                team:1
              })
              const game_time = Math.floor(result_match.duration / 60) + ':' + result_match.duration % 60
              const deletRoom = async () => {
          		await duelsDota.remove({_id:room_id})
              }

              if(checkUserTeam0 && checkUserTeam1 && findMember0 && findMember1){

                const accountId0 = team0[0].account_id
                const accountId1 = team1[0].account_id
                let checkHero0 = undefined
                let checkHero1 = undefined

                const findPlayer0 = _.findWhere(result_match.players, {
                  account_id:accountId0
                })

                const findPlayer1 = _.findWhere(result_match.players, {
                  account_id:accountId1
                })

                if(findPlayer0){
                  checkHero0 = _.findWhere(findRoom.random_heros, {
                    id:findPlayer0.hero_id,
                    banStatus:false
                  })
                }

                if(findPlayer1){
                  checkHero1 = _.findWhere(findRoom.random_heros, {
                    id:findPlayer1.hero_id,
                    banStatus:false
                  })
                }

                if(checkHero1 == undefined && checkHero0
                  || checkHero0 && result_match.radiant_score == 2 && result_match.radiant_win 
                  || findRoom.type_game == "ALL PICK" && result_match.radiant_score == 2 && result_match.radiant_win
                  || checkHero0 && result_match.tower_status_radiant > 2039 && result_match.radiant_win
                  || findRoom.type_game == "ALL PICK" && result_match.tower_status_radiant > 2039 && result_match.radiant_win
                  ){
                  
                  await dotaRating.updateMany({user_id:findRoom.team0[0]}, { $inc: { score: +1, count_games: +1 } });
                  await dotaRating.updateMany({user_id:findRoom.team1[0]}, { $inc: { count_games: +1 } });
            
                  if(findRoom.currency == "dollars"){
                    await balance.updateMany({user:findRoom.team0[0]},{$inc: { balance: +(findRoom.bet *.95) }})
                    await balance.updateMany({user:findRoom.team1[0]},{$inc: { balance: -findRoom.bet }})

                  }else{
                    await balance.updateMany({user:findRoom.team0[0]},{$inc: { coins: +(findRoom.bet *.95) }})
                    await balance.updateMany({user:findRoom.team1[0]},{$inc: { coins: -findRoom.bet }})
                  }
                  const rangTeam0 = await levels.findOne({user_id:findRoom.team0[0]});
                  const rangTeam1 = await levels.findOne({user_id:findRoom.team1[0]});
                  await levels.updateMany({user_id:findRoom.team0[0]}, { rang:getRang(+(rangTeam0.score)+1), $inc: { score: +1 } });
                  await levels.updateMany({user_id:findRoom.team1[0]}, { rang:getRang(+(rangTeam1.score)+1), $inc: { score: +1 } });

                  let findAllHeros = await dotaHeros.find()
                  let getHeroTeam0 = _.findWhere(findAllHeros[0].dota_heros, {
                  	id: findPlayer0.hero_id
                  })
                  let getHeroTeam1 = _.findWhere(findAllHeros[0].dota_heros, {
                  	id: findPlayer1.hero_id
                  })
                  await resultDotaGames.create({
                  	hero:getHeroTeam0, 
                  	user_id:findRoom.team0[0],
                  	type_game:"OneVsOne",
                  	game_time,
                  	game_result: 'Победа',
                  	count_score:findPlayer0.kills,
                  	count_deaths:findPlayer0.deaths
                  })

                  await resultDotaGames.create({
                  	hero:getHeroTeam1, 
                  	user_id:findRoom.team1[0],
                  	type_game:"OneVsOne",
                  	game_time,
                  	game_result: 'Поражение',
                  	count_score:findPlayer1.kills,
                  	count_deaths:findPlayer1.deaths
                  })

                }else if(checkHero0 == undefined && checkHero1 
                  || checkHero1 && result_match.dire_score == 2 && result_match.radiant_win == false 
                  || findRoom.type_game == "ALL PICK" && result_match.dire_score == 2 && result_match.radiant_win == false 
                  || findRoom.type_game == "ALL PICK" && result_match.tower_status_dire > 2039 && result_match.radiant_win == false
                  || checkHero1 && result_match.tower_status_dire > 2039 && result_match.radiant_win == false){

                  await dotaRating.updateMany({user_id:findRoom.team1[0]}, { $inc: { score: +1, count_games: +1 } });
                  await dotaRating.updateMany({user_id:findRoom.team0[0]}, { $inc: { count_games: +1 } });
                  
                  if(findRoom.currency == "dollars"){
                    await balance.updateMany({user:findRoom.team1[0]},{$inc: { balance: +(findRoom.bet *.95) }})
                    await balance.updateMany({user:findRoom.team0[0]},{$inc: { balance: -findRoom.bet }})

                  }else{

                    await balance.updateMany({user:findRoom.team1[0]},{$inc: { coins: +(findRoom.bet *.95) }})
                    await balance.updateMany({user:findRoom.team0[0]},{$inc: { coins: -findRoom.bet }})
                  }

                  const rangTeam0 = await levels.findOne({user_id:findRoom.team0[0]});
                  const rangTeam1 = await levels.findOne({user_id:findRoom.team1[0]});
                  await levels.updateMany({user_id:findRoom.team0[0]}, { rang:getRang(+(rangTeam0.score)+1), $inc: { score: +1 } });
                  await levels.updateMany({user_id:findRoom.team1[0]}, { rang:getRang(+(rangTeam1.score)+1), $inc: { score: +1 } });

                  let findAllHeros = await dotaHeros.find()
                  let getHeroTeam0 = _.findWhere(findAllHeros[0].dota_heros, {
                  	id: findPlayer0.hero_id
                  })
                  let getHeroTeam1 = _.findWhere(findAllHeros[0].dota_heros, {
                  	id: findPlayer1.hero_id
                  })
                  
                  await resultDotaGames.create({
                  	hero:getHeroTeam1, 
                  	user_id:findRoom.team1[0],
                  	type_game:"OneVsOne",
                  	game_time,
                  	game_result: 'Победа',
                  	count_score:findPlayer1.kills,
                  	count_deaths:findPlayer1.deaths
                  })

                  await resultDotaGames.create({
                  	hero:getHeroTeam0, 
                  	user_id:findRoom.team0[0],
                  	type_game:"OneVsOne",
                  	game_time,
                  	game_result: 'Поражение',
                  	count_score:findPlayer0.kills,
                  	count_deaths:findPlayer0.deaths
                  })
                }else{
                  deletRoom()
                  ctx.status = 200
                  return ctx.body = 'Игра не по правилам!'
                }

              }else{
                deletRoom()
                ctx.status = 200
                return ctx.body = 'Игра не по правилам!'
              }
                
              deletRoom()
            }else if(findRoom.type_duel == "TWO_BY_TWO" && result_match.match_id){

            }else{
              ctx.status = 200
              return ctx.body = 'ERROR'
            }

            ctx.status = 200
            return ctx.body = "Ok"
          }else{
            ctx.status = 200
            return ctx.body ='Матч еще не закончен!'
          }
        }else{
          ctx.status = 200
          return ctx.body ='Матч еще не начат!'
        }      
      }else{
        ctx.status = 200
        return ctx.body = 'ERROR1'
      }
    }catch(err){
      console.log(122233, err)
      ctx.status = 200
      return ctx.body ='Матч еще не закончен! Вы не можите покинуть комнату!'
    }
  },

  inviteToLobby: async (ctx)=>{
    const {
      idLobby
    } = ctx.params

    let room_id = null
    //Dota2.inviteToLobby("76561198863031462");
    Dota2.launchPracticeLobby(()=>{
      Dota2.leavePracticeLobby((err, data)=>{
        if (!err) {
          console.log('LEAVE LOBBY', data)
          Dota2.abandonCurrentGame();
        } else {
          console.log('ERROR', err)
        }
      });
    }) 

    /* //Dota2.inviteToLobby("76561198077191513");
    
    Dota2.joinPracticeLobby(idLobby, "123", (data)=>{
      console.log(455, data)
    })*/
    ctx.status = 200
    return ctx.body = {
      data:"OK"
    }
  },

  createBot: async (ctx)=>{
    let {bot_name, login, password, bot_type, league_id, port } = ctx.request.body
    if(bot_name && login && password && bot_type && league_id && port){
      await bots.create({bot_name, login, password, bot_type, league_id, port})
      ctx.status = 200
      return ctx.body = "OK"
    }else{
      ctx.status = 400
      return ctx.body = "ERROR"
    }
  }
}