import {
	users,
	resultGames,
	teams
} from '../../../../models'
import _ from 'underscore'
import fs from 'fs'
import randtoken from 'rand-token'
//import path from 'path';

export default {


	createTeam: async (ctx, next) => {
		let {teamName, abreviation} = ctx.query
		let { user, imageName } = ctx
		console.log(4354555, imageName)
		if(imageName && teamName && abreviation){

			try{
				let createTeam = await teams.create({
					leader_id:user._id,
					team_name:teamName,
					team_logo:imageName,
					activeTeam:user._id,
					abreviation
				});

				let userUpdate = await users.updateOne({_id:user._id}, {team:createTeam._id})
				
				ctx.status = 200
				
				return ctx.body = createTeam
			}catch(err){
				ctx.status = 500
				console.log(err)
				return ctx.body = 'ERROR'
			}
		}else{
			console.log(500, 'ERR')
			ctx.status = 500
			return ctx.body = 'Запоните все поля'
		}
	},

	getTeam: async (ctx, next) => {
		let { user } = ctx

		try{
			let teamId = await users.findOne({_id: user._id})

			if(teamId.team){
				let findTeam = await teams.findOne({_id: teamId.team}).deepPopulate('activeTeam reservTeam activeTeam.level reservTeam.level');
				
				//let findResults = await resultGames.findAll({team_id: teamId.team})
				//console.log('resultGames', resultGames)
				ctx.status = 200
				return ctx.body = {
					team:findTeam,
					//results:findResults
				}
			}else{
				ctx.status = 200
				return ctx.body = null
			}
		}catch(err){
			ctx.status = 500
			console.log("ERROR-->>", err)
		}
	},

	removeTeam: async (ctx, next) => {
		let { user } = ctx
		
		try{

			if (user.team) {
				const team = await teams.findOne({_id:user.team})
				
				await users.updateMany({team:user.team}, {team:user._id})
				await teams.remove({_id: user.team})
				
				if(team.team_logo){
					fs.unlinkSync("/var/ggApi/public/uploads/teamLogos/"+team.team_logo);
				}
				
				//let findResults = await resultGames.findAll({team_id: user.team})
				ctx.status = 200
				return ctx.body = 'ok'
			} else {
				ctx.status = 200
				return ctx.body = null
			}
		} catch (err) {
			ctx.status = 500
			console.log("ERROR-->>", err)
			return ctx.body = null
		}
	},

	outTeam: async (ctx, next) => {
		let { user } = ctx

		try{
			const findTeam = await teams.findOne({_id:user.team})
			let updateUserTeam = await users.updateOne({_id:user._id}, {team:user._id})

			findTeam.activeTeam = findTeam.activeTeam.filter((item)=>{
				if(`${item}` !== `${user._id}`){
					return item
				}
			})
			findTeam.reservTeam = findTeam.reservTeam.filter((item)=>{
				if(`${item}` !== `${user._id}`){
					return item
				}
			})

			await teams.updateOne({_id:findTeam._id},{
				activeTeam:findTeam.activeTeam,
				reservTeam:findTeam.reservTeam
			})

			ctx.status = 200
			return ctx.body = 'ok'
		} catch (err) {
			ctx.status = 500
			console.log("ERROR-->>", err)
		}
	},

	swithcUser: async (ctx, next) => {
		let { user } = ctx
		let {id, index, sostavTeam} = ctx.request.body

		if (id && sostavTeam) {
			
			try{
				let findTeam = await teams.findOne({leader_id:user._id})

				if(sostavTeam === 'reservTeam'){
					findTeam.reservTeam.splice(index, 1)
					findTeam.activeTeam.push(id)

					await teams.updateOne({_id:findTeam._id}, {
						activeTeam: findTeam.activeTeam,
						reservTeam: findTeam.reservTeam
					})
				}else{
					findTeam.activeTeam.splice(index, 1)
					findTeam.reservTeam.push(id)

					await teams.updateOne({_id:findTeam._id}, {
						activeTeam: findTeam.activeTeam,
						reservTeam: findTeam.reservTeam
					})
				}
				let findUpdateTeam = await teams.findOne({leader_id:user._id}).deepPopulate('activeTeam reservTeam activeTeam.level reservTeam.level');
				ctx.status = 200
				return ctx.body = findUpdateTeam
			} catch (err) {
				ctx.status = 500
				console.log("ERROR-->>", err)
			}
		}else{
			ctx.status = 500
			return ctx.body = 'Запоните все поля'
		}
	},
	
}