import {
	users,
  balance,
	dotaRating,
  prestige,
  resultDotaGames
} from '../../../../models'
import moment from 'moment'
import _ from 'underscore'
import randtoken from 'rand-token'
import config from 'config'
import request from 'request'
import jwt from 'jsonwebtoken'

const algorithm = 'HS512'

export default {
	
	getAllRating: async (ctx, next) => {
		try{
			let getRatingTop = await dotaRating.find({}, [], {
		    sort:{
	        score: -1
		    },
		    limit:10				
			}).deepPopulate('user_id user_id.level');

			ctx.status = 200
			return ctx.body = getRatingTop
		}catch(err){
			ctx.status = 500
			return ctx.body = 'Ошибка. Попробуйте позже'
		}
	},

  getProfile: async (ctx, next) => {
  	let { user } = ctx
    try{
      const {
        id
      } = ctx.params

      let findUser = await users.findOne({_id:id}).deepPopulate('level dota_score')

      if(findUser){
        let findbalance = await balance.findOne({user:user._id})
        let getPrestige = await prestige.findOne({
          user:user._id
        })

        let allDotaGames = await resultDotaGames.find({user_id:id})

      	if(String(findUser._id) == String(user._id)){
         
          if(findbalance){
            findUser = {...findUser._doc, 
              prestige: getPrestige ? true : null, 
              balance: Math.floor(findbalance.balance),
              coins: Math.floor(findbalance.coins)
            }
          }else{
            await balance.create({user:id})
          }

      	}
        ctx.status = 200
        console.log(11111, findUser)
        return ctx.body = {
          user:findUser,
          allDotaGames,
        }
      }else{
        ctx.status = 400
        return ctx.body = null
      }
    }catch(err){
      ctx.status = 400
      return ctx.body = 'Ошибка. Попробуйте позже'
    }
  },
}