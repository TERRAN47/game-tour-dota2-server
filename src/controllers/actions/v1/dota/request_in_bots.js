import {
  users,
  duelsDota,
  levels,
  dotaHeros,
  resultDotaGames,
  bots,
  tournamGames,
  dotaRating
} from '../../../../models'

export default {

  updUserAccountId: async (ctx) => {
    const {account_id, _id} = ctx.request.body
    if(account_id &&  _id){
      await users.updateOne({_id}, {
        account_id
      })
      ctx.status = 200
      return ctx.body = "Ok"
    }else{
      ctx.status = 400
      return ctx.body = 'Делать нечего?'
    }
  },
  tournamNewLobby: async (ctx) => {
    const {update, _id} = ctx.request.body
    if(update && _id){
      await tournamGames.updateOne({_id}, update);
      ctx.status = 200
      return ctx.body = "Ok"
    }else{
      ctx.status = 400
      return ctx.body = 'Делать нечего?'
    }
  },
  updateOneBot: async (ctx) => {
    const {status, _id} = ctx.request.body

    if(status && _id){

      await bots.updateOne({_id}, {status})
      ctx.status = 200
      return 200
    }else{

      ctx.status = 400
      return ctx.body = 'Делать нечего?'
    }
  },



}