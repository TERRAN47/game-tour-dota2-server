import {
	users,
	dotaHeros,
	bots,
	balance,
	duelsDota
} from '../../../../models'
import moment from 'moment'
import _ from 'underscore'
import request from 'request'

export default {

	updateDuelRoom:async (ctx, next) => {

		const {
			room_id
		} = ctx.params

		if(room_id){
			try{
				let findRoom = await duelsDota.findOne({_id:room_id}).deepPopulate('team0 team1 team1.level team0.level team0.dota_score team1.dota_score')
				
				return ctx.body = {
					data:findRoom
				}
			}catch(err){

				ctx.status = 500
				return ctx.body = 'Ошибка. Попробуйте позже'
			}			
		}else{
			ctx.status = 500
			return ctx.body = 'Ошибка. Нет данных'	
		}
	},
	
	checkFreeBot: async (ctx) =>{
    let {room_id, team0, team1} = ctx.request.body
    const { user, findRoom } = ctx

    try{
    	if(room_id){
	    	if(findRoom && team0.length > 0 && team1.length > 0){
		    	if(findRoom.count_create_loby < 4 && findRoom.match_id == null){
		    		findRoom._doc.hoster_id = String(findRoom._doc.hoster_id)
			    	if(findRoom.bot_id == null || findRoom.bot_id == undefined){
			    		const findFreeBot = await bots.findOne({bot_type:"duel", status:true})
				    	if(findFreeBot){
				   
				    		await request.post(`https://2.59.41.95:${findFreeBot.port}/v1/dota/dota-launcher/create-lobby`).form({
								room_id,
								team0,
								team1,
								findRoom:findRoom._doc,
								findFreeBotId:String(findFreeBot._id),
								leagueid:findFreeBot.league_id,
								user_id:String(user._id),
								botLogin: findFreeBot._doc.login,
								botPassword: findFreeBot._doc.password
					    	})
				    	}else{
								ctx.status = 200
								return ctx.body = 'Извините попробуйте через пару минут, все лобби заняты'  
				    	} 
			    	}else{
			    		if(findRoom.bot_id.room_id == room_id){
			    			
				    		await request.post(`https://2.59.41.95:${findRoom.bot_id.port}/v1/dota/dota-launcher/create-lobby`).form({
									room_id,
									team0,
									team1,
									findRoom:findRoom._doc,
									findFreeBotId:String(findRoom.bot_id._id),
									restartLoby:true,
									leagueid:findRoom.bot_id.league_id,
									user_id:String(user._id),
									botLogin: findRoom.bot_id.login,
									botPassword: findRoom.bot_id.password
					    	})
			    		}else{
				    		const findFreeBot = await bots.findOne({bot_type:"duel", status:true})
				    	
					    	if(findFreeBot){
					    		await request.post(`https://2.59.41.95:${findFreeBot.port}/v1/dota/dota-launcher/create-lobby`).form({
										room_id,
										team0,
										team1,
										findRoom:findRoom._doc,
										findFreeBotId:String(findFreeBot._id),
										leagueid:findFreeBot.league_id,
										user_id:String(user._id),
										botLogin: findFreeBot._doc.login,
										botPassword: findFreeBot._doc.password
						    	})
					    	}else{
									ctx.status = 200
									return ctx.body = 'Извините попробуйте через пару минут, все лобби заняты'  
					    	} 
			    		}

			    	}
			    	ctx.status = 200
						return ctx.body = 'OK'
			    }else{
			      ctx.status = 200
			      console.log(222, 'LIMIT')
			      return ctx.body =  findRoom.match_id == null ? 'Вы привысили лимит создания нового лоби!' : 'Матч уже начался!'
			    }
	    	}else{
					ctx.status = 500
					console.log(123)
					return ctx.body = 'Ошибка. Попробуйте позже'  
	    	}
    	}else{
				ctx.status = 500
				console.log(777)
				return ctx.body = 'Ошибка. Попробуйте позже'  
    	}
		}catch(err){
			console.log(66666, err)
			ctx.status = 500
			return ctx.body = 'Ошибка. Попробуйте позже'  
		}

	},

	getDuelsRooms: async (ctx, next) => {

		let { user } = ctx

		try{
			let checkGamerInRoom = await duelsDota.findOne({
				$or:[{
					'team0.0':user._id
				},{
					'team0.1':user._id
				},{
					'team1.0':user._id
				},{
					'team1.1':user._id
				}]
			}).deepPopulate('team0 team1 team1.level team0.level team0.dota_score team1.dota_score');

			ctx.status = 200;

			if(checkGamerInRoom){			
				return ctx.body = {
					inRoom:checkGamerInRoom
				}
			}else{
				let getRooms = await duelsDota.find().deepPopulate('hoster_id hoster_id.level hoster_id.dota_score');
				return ctx.body = {
					data:getRooms
				}
			}
		}catch(err){
			ctx.status = 500
			return ctx.body = 'Ошибка. Попробуйте позже'
		}
	},

	getRandomHeros: async (ctx, next) => {
		const {
			room_id
		} = ctx.params;

		if(room_id){
			try{
				let dotaHeroes = await dotaHeros.findOne()
				let allHeroes = dotaHeroes.dota_heros
				let item
				let randomHeros = [];

				for (var i = 1; i <= 5; i++) {
					item = allHeroes[Math.floor(Math.random()*allHeroes.length)];
					randomHeros.push({...item, banStatus:false})
				}

				let findUpdateDuel = await duelsDota.findOneAndUpdate({_id:room_id}, 
				{random_heros:randomHeros}, {new:true }).deepPopulate('team0 team1 team1.level team0.level team0.dota_score team1.dota_score')

				ctx.status = 200
				return ctx.body = {
					data:findUpdateDuel
				}
			}catch(err){
				ctx.status = 500
				return ctx.body = 'Ошибка. Попробуйте позже'
			}			
		}else{
			ctx.status = 500
			return ctx.body = 'Ошибка. Попробуйте позже'			
		}
	},

	banHero: async (ctx, next) => {
		let { user } = ctx
		let {heroName, room_id} = ctx.request.body
		if(heroName && room_id){
			try{
				let findRoom = await duelsDota.findOne({_id:room_id})
				
				if(findRoom){
					let findActiveHeroes = _.where(findRoom.random_heros, {
						banStatus:false
					})
					if(findRoom.match_id == null && findActiveHeroes.length > 3){
						let findHero = _.findWhere(findRoom.random_heros, {
							name:heroName
						})

						findHero.banStatus = true

						let findUpdateDuel = await duelsDota.findOneAndUpdate({_id:room_id}, {random_heros:findRoom.random_heros}, {new:true })
						.deepPopulate('team0 team1 team1.level team0.level team0.dota_score team1.dota_score')
						ctx.status = 200
						return ctx.body = {
							data:findUpdateDuel
						}
					}else{
						console.log(123)
						ctx.status = 200
						return ctx.body = {
							gameStart:"Бан героев закончен!"
						}
					}
				}else{
					console.log(444)
					ctx.status = 500
					return ctx.body = {
						data:"Ошибка 500"
					}					
				}				
			}catch(err){
				console.log(11, err)
				ctx.status = 500
				return ctx.body = {
					data:"Ошибка 500"
				}	
			}
		}else{
			console.log(555)
			ctx.status = 500
			return ctx.body = {
				data:"Ошибка 500"
			}			
		}
	},

	createDuelRoom: async (ctx, next) => {
		let { user } = ctx
		let {bet, currency, type_game, type_duel} = ctx.request.body
		if(bet, currency, type_duel){
			try{
				let findRoom = await duelsDota.findOne({'hoster_id':user._id})

				if(findRoom){
					ctx.status = 200
					return ctx.body = {
						alert:"У вас уже есть активный сервер"
					}
				}else{
					let userBalance = await balance.findOne({user:user._id})
					console.log(111111, user._id)
					console.log(1233333, userBalance)
					if(currency == "dollars"){
						if(userBalance.balance >= bet){
							let createRoom = await duelsDota.create({
								hoster_id:user._id,
								team0:user._id,
								bet,
								type_game,
								type_duel,
								team0_ready:[1],
								team1_ready:[0],
								currency
							});
							
							createRoom.team0[0] = user

							ctx.body = 200
							return ctx.body = {
								data:createRoom
							}
						}else{
							return ctx.body = {
								alert:"Пополните баланс!"
							}
						}
					}else if(currency == "couns"){
						if(userBalance.coins >= bet){
							let createRoom = await duelsDota.create({
								hoster_id: user._id,
								team0: user._id,
								bet,
								type_game,
								type_duel,
								team0_ready:[1],
								team1_ready:[0],
								currency
							});
							
							createRoom.team0[0] = user

							ctx.body = 200
							return ctx.body = {
								data:createRoom
							}
						}else{
							return ctx.body = {
								alert:"Пополните коины!"
							}
						}
					}else{
						return ctx.body = {
							alert:"Выберите валюту!"
						}
					}
				}
			}catch(err){
				ctx.status = 500
				console.log(44454, err)
				return ctx.body = {
					err:"Ошибка 500"
				}
			}
		}else{
			ctx.status = 500
			console.log(666, "err")
			return ctx.body = {
				err:".!."
			}
		}
	},

	deleteDuelRoom: async (ctx, next) => {
		const {
			room_id
		} = ctx.params
		const { user } = ctx
		if(room_id){
			try{

				let reqestRoom = await duelsDota.remove({'hoster_id':user._id, _id:room_id})

				if(reqestRoom.deletedCount > 0){
					ctx.status = 200
					return ctx.body = {
						data:'Успешно удалено',
						err:false
					}
				}else{
					ctx.status = 500
					return ctx.body = {
						err:true
					}	
				}
			}catch(err){
				ctx.status = 500
				return ctx.body = {
					err:true
				}
			}
		}else{
			ctx.status = 500
			return ctx.body = {
				err:".!."
			}
		}						
	},

	kickGamer: async (ctx, next) => {
		const {
			index, team
		} = ctx.request.body

		const { user } = ctx
		try{
			let findRoom = await duelsDota.findOne({'hoster_id':user._id}).deepPopulate('team0 team1 team1.level team0.level team0.dota_score team1.dota_score')
			if(findRoom && team){
				if(team == 'team0'){
					findRoom.team0_ready[index] = 0
					findRoom.team0.splice(index, 1)
				}else{
					findRoom.team1_ready[index] = 0
					findRoom.team1.splice(index, 1)
				}
				if(Array.isArray(findRoom.team0) && Array.isArray(findRoom.team1) &&  Array.isArray(findRoom.team0_ready) && Array.isArray(findRoom.team1_ready)){
					let resultUpdate = await duelsDota.updateOne({hoster_id:user._id}, {
						team0:findRoom.team0, 
						team1:findRoom.team1, 
						team0_ready:findRoom.team0_ready, 
						team1_ready:findRoom.team1_ready
					})
					
					ctx.status = 200
					return ctx.body = {
						data:findRoom,
					}					
				}else{
					ctx.status = 500
					return ctx.body = {
						err:".!."
					}
				}
			}else{

				ctx.status = 500
				return ctx.body = {
					err:".!."
				}			
			}
		}catch(err){
			console.log(5677, err)
			ctx.status = 500
			return ctx.body = {
				err:".!."
			}
		}		
  },
  
	leaveDuelRoom: async (ctx, next) => {
		const {
			room_id
		} = ctx.params;
		const { user } = ctx
		if(room_id){
			try{
				let findRoom = await duelsDota.findOne({_id:room_id})
			
				if(findRoom){
					let team0 = findRoom.team0
					let team1 = findRoom.team1
					let team0Ready = findRoom.team0_ready
					let team1Ready = findRoom.team1_ready

					switch(String(user._id)){
						case String(team0[0]) :
							team0Ready[0] = 0
							team0.splice(0, 1)
						break;

						case String(team0[1]) :
							team0Ready[1] = 0
							team0.splice(1, 1)
						break;

						case String(team1[0]) :
							team1Ready[0] = 0
							team1.splice(0, 1)
						break;

						case String(team1[1]) :
							team1Ready[1] = 0
							team1.splice(1, 1)
						break;

						default :
							return 'ERROR'
					}

					await duelsDota.updateOne({_id:room_id}, {
						team0, 
						team1, 
						team0_ready:team0Ready, 
						team1_ready:team1Ready
					})

					return ctx.body = {
						data:'Успешный выход'
					}					
				}else{
					ctx.status = 500
					return ctx.body = {
						err:".!."
					}
				}
			}catch(err){
				ctx.status = 500
				return ctx.body = {
					err:true
				}
			}
		}else{
			ctx.status = 500
			return ctx.body = {
				err:".!."
			}
		}						
	},

	startReady: async (ctx, next) => {
		const { user } = ctx
		const {
			room_id
		} = ctx.params

		try{
			let findRoom = await duelsDota.findOne({_id:room_id})

			if(findRoom){
				let team0 = findRoom.team0
				let team1 = findRoom.team1
				let team0Ready = findRoom.team0_ready
				let team1Ready = findRoom.team1_ready

				switch(String(user._id)){
					case String(team0[0]) :
						team0Ready[0] = 1
					break;

					case String(team0[1]) :
						team0Ready[1] = 1
					break;

					case String(team1[0]) :
						team1Ready[0] = 1
					break;

					case String(team1[1]) :
						team1Ready[1] = 1
					break;

					default :
						return 'ERROR'
				}

				await duelsDota.updateOne({_id:room_id}, {
					team0_ready:team0Ready, 
					team1_ready:team1Ready
				})
				ctx.status = 200
				return ctx.body = {
					data:"ok"
				}
			}else{
				ctx.status = 500
				return ctx.body = {
					data:".!."
				}
			}
		}catch(err){
			console.log(5677, err)
			ctx.status = 500
			return ctx.body = {
				data:".!."
			}
		}
	},
	switchSlot:async (ctx, next) => {
		const {
			slot_id
		} = ctx.params
		const { user } = ctx
		if(slot_id){
			try{
				let findInRoom = await duelsDota.findOne({
					$or:[{
						'team0.0':user._id
					},{
						'team0.1':user._id
					},{
						'team1.0':user._id
					},{
						'team1.1':user._id
					}]
				})

				if(findInRoom){
					let team0 = findInRoom.team0
					let team1 = findInRoom.team1
					let team0Ready = findInRoom.team0_ready
					let team1Ready = findInRoom.team1_ready

					let switchNewSlot = (team, teamReady, index)=>{
						teamReady[index] = 0;
						team.splice(index, 1);

						if(slot_id == "team0Slot1"){
							if(team0.length == 0){
								if(String(findInRoom.hoster_id) == String(user._id)){
									team0Ready[0] = 1;
								}else{
									team0Ready[0] = 0;
								}
								team0[0] = user._id
							}else{
								if(String(findInRoom.hoster_id) == String(user._id)){
									team0Ready[1] = 1;
								}else{
									team0Ready[1] = 0;
								}
								
								team0[1] = user._id
							}
						}else{
							if(team1.length == 0){
								if(String(findInRoom.hoster_id) == String(user._id)){
									team1Ready[0] = 1;
								}else{
									team1Ready[0] = 0;
								}
								
								team1[0] = user._id
							}else{
								if(String(findInRoom.hoster_id) == String(user._id)){
									team1Ready[1] = 1;
								}else{
									team1Ready[1] = 0;
								}
								team1Ready[1] = 0;
								team1[1] = user._id
							}
						}
						return					
					}

					switch(String(user._id)){
						case String(team0[0]) :
							switchNewSlot(team0, team0Ready, 0)
						break;

						case String(team0[1]) :
							switchNewSlot(team0, team0Ready, 1)
						break;

						case String(team1[0]) :
							switchNewSlot(team1, team1Ready, 0)
						break;

						case String(team1[1]) :
							switchNewSlot(team1, team1Ready, 1)
						break;

						default :
							return 'ERROR'
					}
					
					let findAndUpdateRoom = await duelsDota.findOneAndUpdate({
						$or:[{
							'team0.0':user._id
						},{
							'team0.1':user._id
						},{
							'team1.0':user._id
						},{
							'team1.1':user._id
						}]
					},{	
						team0, 
						team1, 
						team0_ready:team0Ready, 
						team1_ready:team1Ready
					},{
						new:true 
					}).deepPopulate('team0 team1 team1.level team0.level team0.dota_score team1.dota_score')
					ctx.status = 200

					return ctx.body = {
						data:findAndUpdateRoom
					}
				}else{
					ctx.status = 500
					return ctx.body = {
						data:".!."
					}
				}			
			}catch(err){
				console.log(4567, err)
				ctx.status = 500
				return ctx.body = {
					data:".!."
				}			
			}
		}else{
			ctx.status = 500
			return ctx.body = {
				data:".!."
			}
		}
  },
  
	checkSlotRoom: async (ctx, next) => {
		const {
			room_id
		} = ctx.params
		const { user } = ctx
		if(room_id){
			try{
				let findRoom = await duelsDota.findOne({_id:room_id})
				
				let team0 = findRoom.team0
				let team1 = findRoom.team1
				let team0Ready = findRoom.team0_ready
				let team1Ready = findRoom.team1_ready
				let findRoomUsers = await duelsDota.findOne({_id:room_id}).deepPopulate('team0 team1 team1.level team0.level team0.dota_score team1.dota_score')
				
				const findRoomDeUsers = async ()=>{
					ctx.status = 200
					return ctx.body = {
						data:findRoomUsers,
						full:false
					}	
				}

				if(String(team0[0]) == String(user._id)){
					return findRoomDeUsers()
				}else{
 
					let userBalance = await balance.findOne({user:user._id})

					if(findRoom.type_duel == "ONE_BY_ONE"){

						if(team0.length == 0){
							if(findRoomUsers.currency == "dollars"){
								if(userBalance.balance >= findRoomUsers.bet){
				 					team0.push(user._id)

									let findAndUpdateRoom = await duelsDota.findOneAndUpdate({_id:room_id}, {team0:team0}, {new:true })
									.deepPopulate('team0 team1 team1.level team0.level team0.dota_score team1.dota_score')
									ctx.status = 200
									return ctx.body = {
										data:findAndUpdateRoom,
										full:false
									}
								}else{
									ctx.status = 200
									return ctx.body = {
										info:'Пополните баланс!',
										full:true
									}
								}
							}else if(findRoomUsers.currency == "couns"){
								if(userBalance.coins >= findRoomUsers.bet){
				 					team0.push(user._id)

									let findAndUpdateRoom = await duelsDota.findOneAndUpdate({_id:room_id}, {team0:team0}, {new:true })
									.deepPopulate('team0 team1 team1.level team0.level team0.dota_score team1.dota_score')
									ctx.status = 200
									return ctx.body = {
										data:findAndUpdateRoom,
										full:false
									}
								}else{
									ctx.status = 200
									return ctx.body = {
										info:'Пополните коины!',
										full:true
									}
								}
							}else{
									ctx.status = 200
									return ctx.body = {
										info:'Выберите валюту!',
										full:true
									}
							}
						}else if(team1.length == 0){
							if(findRoomUsers.currency == "dollars"){
								if(userBalance.balance >= findRoomUsers.bet){
				 					team1.push(user._id)

									let findAndUpdateRoom = await duelsDota.findOneAndUpdate({_id:room_id}, {team1:team1}, {new:true })
									.deepPopulate('team0 team1 team1.level team0.level team0.dota_score team1.dota_score')

									ctx.status = 200
									return ctx.body = {
										data:findAndUpdateRoom,
										full:false
									}
								}else{
									ctx.status = 200
									return ctx.body = {
										info:'Пополните баланс!',
										full:true
									}
								}
							}else if(findRoomUsers.currency == "couns"){
								if(userBalance.coins >= findRoomUsers.bet){
				 					team1.push(user._id)

									let findAndUpdateRoom = await duelsDota.findOneAndUpdate({_id:room_id}, {team1:team1}, {new:true })
									.deepPopulate('team0 team1 team1.level team0.level team0.dota_score team1.dota_score')

									ctx.status = 200
									return ctx.body = {
										data:findAndUpdateRoom,
										full:false
									}
								}else{
									ctx.status = 200
									return ctx.body = {
										info:'Пополните коины!',
										full:true
									}
								}
							}else{
									ctx.status = 200
									return ctx.body = {
										info:'Выберите валюту!',
										full:true
									}
							}
						}else{
							if(String(team1[0]) == String(user._id)){
								return findRoomDeUsers()
							}else{
								ctx.status = 200
								return ctx.body = {
									info:'Комната полная',
									full:true
								}		
							}
						}
					}else if(findRoom.type_duel == "TWO_BY_TWO"){
						if(team0.length < 2){
		 					team0.push(user._id)
		 					team0Ready.push(0)
		 					team1Ready.push(0)

							let findAndUpdateRoom = await duelsDota.findOneAndUpdate({_id:room_id}, {team0:team0}, {new:true })
							.deepPopulate('team0 team1 team1.level team0.level team0.dota_score team1.dota_score')

							ctx.status = 200
							return ctx.body = {
								data:findAndUpdateRoom,
								full:false
							}
						}else if(team1.length < 2){
		 					team1.push(user._id)
		 					team0Ready.push(0)
		 					team1Ready.push(0)

		 					let findAndUpdateRoom = await duelsDota.findOneAndUpdate({_id:room_id}, {team1:team1}, {new:true })
		 					.deepPopulate('team0 team1 team1.level team0.level team0.dota_score team1.dota_score')

							ctx.status = 200
							return ctx.body = {
								data:findAndUpdateRoom,
								full:false
							}	
						}else{
							if(String(team1[0]) == String(user._id) || String(team1[1]) == String(user._id)){
								return findRoomDeUsers()
							}else{
								ctx.status = 200
								return ctx.body = {
									info:'Комната полная',
									full:true
								}		
							}	
						}
					}else{
						ctx.status = 500
						return ctx.body = {
							err:"Ошибка 500"
						}
					}
				}	
			}catch(err){
				console.log(56, err)
				ctx.status = 500
				return ctx.body = {
					err:true
				}
			}
		}else{
			ctx.status = 500
			return ctx.body = {
				err:".!."
			}
		}		
  },
  

}