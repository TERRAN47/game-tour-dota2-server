import {
  users,
  tournamGames,
  dotaHeros,
  dotaRating,
  resultDotaGames,
  balance,
  teams,
  levels,
  bots,
  tournaments
} from '../../../../models'
import _ from 'underscore'
import tournamActions from './tournament.actions'
import request from 'request'
import {getRang} from "../../../../utils/constans"

export default {
  getTournament: async (ctx, next) => {

    try{
      let findTournaments = await tournaments.find({game:'DOTA2'}, {}, {
        sort:{
          createdAt: -1
        }
      }).deepPopulate('players players.level players.dota_score teams');

    
      ctx.status = 200;
      return ctx.body = findTournaments
    }catch(err){

      ctx.status = 500
      return ctx.body = "ERROR"
    }
  },
  getRoomGame: async (ctx) => {

    try{
      const { user } = ctx
      console.log(12312, user)
      const {
        tournam_id
      } = ctx.params

      const findTournamGames = await tournamGames.findOne({
        tournament_id: tournam_id,
        status: true,
        $or:[{
          'team0.0': user._id
        },{
          'team1.0': user._id
        }]
      }).deepPopulate('team0 team1 team1.level team0.level team0.dota_score team1.dota_score');   

      ctx.status = 200;
      return ctx.body = findTournamGames
    }catch(err){

      ctx.status = 500
      return ctx.body = null
    }
  },
  restartTournaments: async () => {
    await tournaments.updateMany({}, {
      status: "WAIT",
      grid: [],
      players: []
    });
    await tournamGames.deleteMany()
  },
  startTournamentTest: async (ctx) => {
    const {
      time
    } = ctx.params
  },
  startTournament: async (time) => {
    // const {
    //  time
    // } = ctx.params
  
    try{
      if(time){
        
        let findTournament = await tournaments.findOne({time, status: "WAIT"}).deepPopulate("players players.level players.dota_score");
        console.log("findTournament", findTournament)
        if(findTournament){
          if(findTournament && findTournament.players.length == findTournament.count_players){
          	let gridOne = []
          	if(findTournament.type_game == "TEAM_VS_TEAM"){
	            gridOne = findTournament.players.map((item, index)=>{ // формирование сетки первого столба команд 
	              if(index === 0 || index%2 == 0){
	                return([{player: item, team: findTournament.teams[index]._id}, {player: findTournament.players[index+1], team: findTournament.teams[index+1]}]);
	              }
	            });
          	}else{
	            gridOne = findTournament.players.map((item, index)=>{ // формирование сетки первого столба игроков
	              if(index === 0 || index%2 == 0){
	                return([item, findTournament.players[index+1]]);
	              }
	            });
          	}
          
            gridOne = await gridOne.filter((item)=>{
              if(item){
                return item
              }
            })
            findTournament.grid = gridOne
            await tournaments.updateOne({_id: findTournament._id},{status: "START", grid: [gridOne]});

            let findRoomsTournament = null
            if(findTournament.type_game == "TEAM_VS_TEAM"){
	            await Promise.all(await gridOne.map(async (item) => {
	              await tournamGames.create({
	                team0: item[0].player,
	                team1: item[1].player,
                  myTeam0: item[0].team,
                  myTeam1: item[1].team,
	                tournament_id: findTournament._id,
	                type_duel: "TEAM_VS_TEAM",
	                round: 1,
	              })
	            }))

	            findRoomsTournament	= await tournamGames.find({tournament_id: findTournament._id}).deepPopulate("team0 team1 myTeam0 myTeam0.activeTeam myTeam1 myTeam1.activeTeam");
            }else{
	            let dotaHeroes = await dotaHeros.findOne();
	            let allHeroes = dotaHeroes.dota_heros; 
	            let hero
	            let randomHeros = [];

	            for (var i = 1; i <= 5; i++) {
	              hero = allHeroes[Math.floor(Math.random()*allHeroes.length)];
	              randomHeros.push({...hero, banStatus:false})
	            }

	            await Promise.all(await gridOne.map(async (item) => {
	              await tournamGames.create({
	                team0: item[0],
	                team1: item[1],
	                tournament_id:findTournament._id,
	                type_duel: "ONE_BY_ONE",
	                round: 1,
	                random_heros:randomHeros
	              })
	            }))

	            findRoomsTournament	= await tournamGames.find({tournament_id:findTournament._id}).deepPopulate("team0 team1 team1.level team0.level team0.dota_score team1.dota_score");
            }

            if(findRoomsTournament && findRoomsTournament.length){
              // let ctx = {
              //  room_id:findRoomsTournament[0]._id,
              //  team0: findRoomsTournament[0].team0,
              //  team1: findRoomsTournament[0].team1
              // }
              // await tournamActions.checkFreeBot(ctx)     
              let countSlotGamers = 0

              const createLoby = async(countl)=>{
                let ctx = {
                  room_id: findRoomsTournament[countl]._id,
                  team0: findRoomsTournament[countl].team0,
                  team1: findRoomsTournament[countl].team1,
                  myTeam0: findRoomsTournament[countl].myTeam0,
                  myTeam1: findRoomsTournament[countl].myTeam1,
                }
                await tournamActions.checkFreeBot(ctx)

                if(countSlotGamers < findRoomsTournament.length){
                  countSlotGamers += 1;
                  setTimeout(()=>{
                    createLoby(countSlotGamers)
                  }, 9000)
                }
              }

              createLoby(countSlotGamers)
            }else{
              console.log(1231233, findRoomsTournament)
            }
          }else{
            if(findTournament){
              await tournaments.updateOne({_id:findTournament._id},{status:"CLOSED"})

              findTournament.players.forEach(async (elem)=>{
                await balance.updateOne({user:elem._id}, 
                  findTournament.currency == "dollar" ? 
                    {$inc: { balance: +parseInt(findTournament.contribution)}} 
                  : 
                    {$inc: { coins: +parseInt(findTournament.contribution)}}
                )
              })
            }
          }
        }
        
        return findTournament
      }else{
        
        return "ERROR"
      }

    }catch(err){
      console.log(err)
      return  "ERROR"
    }
  },
  checkComplitGames: async () => {
    
    try{
      const findTournament = await tournaments.findOne({status:"START"}).deepPopulate("players players.level players.dota_score teams");
      
      if(findTournament){
        const findAllGames = await tournamGames.find({tournament_id:findTournament._id}).deepPopulate("team0 team1 team1.level team0.level team0.dota_score team1.dota_score");
        
        if(findAllGames && findAllGames.length){
          const findComplitGames = _.where(findAllGames, {status: false});
          const findActiveGames = _.where(findAllGames, {status: true});
          console.log(77777, findActiveGames)
          if(findActiveGames && findActiveGames.length){
            findActiveGames.map(async (item, index) => {
            
              if(item.team_victory == -1){
                
                if(item.match_id){

                  await request(`https://api.opendota.com/api/matches/${item.match_id}`, async (err, res, body) => {
            
                    if(body.error == undefined){
                      let ctx = {
                        findRoom: findActiveGames[index],
                        result_match: JSON.parse(body),
                        findTournament: findTournament._doc,
                      }
                    
                      await tournamActions.resultMatch(ctx)
                    }
                  });
                }else{

                  let ctx = {
                    findRoom: findActiveGames[index],
                    findTournament: findTournament._doc
                  }

                  await tournamActions.resultMatch(ctx)
                }
              }
            });
          }else{

            if(findAllGames.length == findComplitGames.length){
              let ctx = {
                findTournament: findTournament._doc,
                findComplitGames: findAllGames
              }

              console.log(333, "TRUE")

              tournamActions.nextRound(ctx)           
            }else{
              console.log(222, "FALSE")
            }
          } 
        }
      }

      return "ok"
    }catch(err){
      console.log("ERROR", err)
      return "ERROR"
    }
  },
  nextRound: async (ctx) =>{
    const {findTournament, findComplitGames} = ctx
    try{
      let tournamentGrid = findTournament.grid
      
      if(findComplitGames.length >= 2){

        let grid = findComplitGames.map((item, index) => {  //Обозначение победы поражения
          if(item.team_victory == 0){
            tournamentGrid[tournamentGrid.length-1][index][0].win = true
            tournamentGrid[tournamentGrid.length-1][index][1].win = false
            return(item.team0[0]);
          }else{
            tournamentGrid[tournamentGrid.length-1][index][1].win = true
            tournamentGrid[tournamentGrid.length-1][index][0].win = false
            return(item.team1[0]);
          }
        })
      
        if(findComplitGames.length == 2){ //Вознагрождение за 3-4 места

          let userThirdPlace = findComplitGames[0].team_victory == 0 ? 
            findComplitGames[0].team1[0]
          : findComplitGames[0].team0[0]

          let userFourthPlace = findComplitGames[1].team_victory == 0 ? 
            findComplitGames[1].team1[0]
          : findComplitGames[1].team0[0]

          const prize = findTournament.prize / 2 / 2 / 2;

          await balance.updateOne({user:userThirdPlace}, {$inc: { balance: +prize }})
          await balance.updateOne({user:userFourthPlace}, {$inc: { balance: +prize }})
        }

        let newGrid = grid.map((item, index)=>{ //новая сетка
          if(index === 0 || index%2 == 0){
            return([{...item, team: findTournament.teams[index]}, {...grid[index+1], team: findTournament.teams[index+1]}]);
          }
        });

        newGrid = newGrid.filter((item)=>{
          if(item){
            return item
          }
        });

        tournamentGrid.push(newGrid);
        await tournaments.updateOne({_id: findTournament._id}, {grid: tournamentGrid});

        let dotaHeroes = await dotaHeros.findOne();
        let allHeroes = dotaHeroes.dota_heros; 
        let hero;
        let randomHeros = [];

        for (var i = 1; i <= 5; i++) {
          hero = allHeroes[Math.floor(Math.random() * allHeroes.length)];
          randomHeros.push({...hero, banStatus:false})
        }

        await tournamGames.deleteMany({tournament_id: findTournament._id})
        await Promise.all(await newGrid.map(async (item) => {
          await tournamGames.create({
            team0: item[0],
            team1: item[1],
            round: tournamentGrid.length,
            tournament_id:findTournament._id,
            type_duel: "ONE_BY_ONE",
            random_heros:randomHeros
          })
        }))

        const findRoomsTournament = await tournamGames.find({tournament_id:findTournament._id}).deepPopulate("team0 team1 team1.level team0.level team0.dota_score team1.dota_score");
  
        if(findRoomsTournament){
          let countSlotGamers = 0

          const createLoby = async(countl)=>{
            console.log(99999, findRoomsTournament[countl]._id)
            let ctx = {
              room_id:findRoomsTournament[countl]._id,
              team0: findRoomsTournament[countl].team0,
              team1: findRoomsTournament[countl].team1
            }
            await tournamActions.checkFreeBot(ctx)

            if(countSlotGamers < findRoomsTournament.length){
              countSlotGamers += 1;
              setTimeout(()=>{
                createLoby(countSlotGamers)
              }, 9000)
            }
          }

          createLoby(countSlotGamers)
        }else{
          console.log(1231233, findRoomsTournament)
        }
      }else{
        const findLastRound = await tournamGames.findOne({tournament_id:findTournament._id});
        if(findLastRound.team_victory == 0){
          tournamentGrid[tournamentGrid.length-1][0][0].win = true
          tournamentGrid[tournamentGrid.length-1][0][1].win = false
        }else{
          tournamentGrid[tournamentGrid.length-1][0][1].win = true
          tournamentGrid[tournamentGrid.length-1][0][0].win = false
        }

        let tournamPriz = findTournament.prize
        let firstPlace = 0
        let secondPlace = 0
        let thirdFourthPlace = 0

        let userFirstPlace = findLastRound.team_victory == 0 ? 
          tournamentGrid[tournamentGrid.length-1][0][0]._id
        : tournamentGrid[tournamentGrid.length-1][0][1]._id

        let userSecondPlace = findLastRound.team_victory == 0 ? 
          tournamentGrid[tournamentGrid.length-1][0][1]._id
        : tournamentGrid[tournamentGrid.length-1][0][0]._id

        for (var i = 1; i <= 2; i++) {
          if(i == 1){
            firstPlace = findTournament.prize / 2
          }else if(i == 2){
            secondPlace = findTournament.prize / 2 / 2
          }
        }

        await balance.updateOne({user: userFirstPlace}, {$inc: { balance: +firstPlace }});
        await balance.updateOne({user: userSecondPlace}, {$inc: { balance: +secondPlace }});

        await bots.updateMany({bot_type: "tournament"}, {status:true})
        await tournaments.updateOne({_id: findTournament._id}, {status:"END", grid: tournamentGrid})
        await tournamGames.deleteMany({tournament_id: findTournament._id})
      }
      return ctx.status = 200
    }catch(err){
      console.log(9009, err)
      return ctx.status = 500
    }
  },

  checkFreeBot: async (ctx) =>{
    let {room_id, team0, team1, myTeam0, myTeam1} = ctx
    const { user } = ctx
    let findFreeBot = null

    try{
      const findRoom = await tournamGames.findOne({_id:room_id})
      if(room_id){
        if(findRoom && team0.length > 0 && team1.length > 0){
          
          findRoom._doc.hoster_id = String(findRoom._doc.hoster_id)
          let counstFindFreeBot = 0

          const findBot = async()=>{
            if(findRoom.bot_id == null || findRoom.bot_id == undefined){
              const findFreeBot = await bots.findOne({bot_type: "tournament", status:true})
              if(findFreeBot){
                await bots.updateOne({_id:findFreeBot._id}, {room_id, status:false})
                console.log(7777777, findFreeBot.port)
                await request.post(`http://89.223.121.251:${findFreeBot.port}/v1/dota/dota-launcher/create-turnam-lobby`).form({
                  room_id: String(room_id),
                  team0,
                  team0_id: String(team0[0]._id),
                  team1,
                  team1_id: String(team1[0]._id),
                  activeTeam0: myTeam0 && myTeam0.activeTeam ? myTeam0.activeTeam : null,
                  activeTeam1: myTeam1 && myTeam1.activeTeam ? myTeam1.activeTeam : null,
                  findFreeBotId: String(findFreeBot._id),
                  leagueid: findFreeBot.league_id,
                  botPassword: findFreeBot._doc.password,
                  botLogin: findFreeBot._doc.login
                })
                return "OK"
              }else{
                counstFindFreeBot += 1

                if(counstFindFreeBot < 3){
                  setTimeout(()=>{
                    findBot()
                  }, 15000) 
                }else{
                  await bots.updateMany({bot_type: "tournament"}, {status:true})
                  findBot()
                }

                return 'Извините попробуйте через пару минут, все лобби заняты'  
              }     
            }else{
              return 'Ошибка. Попробуйте позже'               
            }
          }
          findBot()
        }else{
          console.log(123)
          return 'Ошибка. Попробуйте позже'  
        }
      }else{
        console.log(777)
        return 'Ошибка. Попробуйте позже'  
      }
    }catch(err){
      console.log(66666, err)
      return 'Ошибка. Попробуйте позже'  
    }
  },

  resultMatch: async (ctx)=>{
    const {
      findRoom,
      findTournament,
      result_match
    } = ctx
    console.log(65656, findRoom)
    try{
      if(findRoom){
        const match_id = findRoom.match_id;
        const lobby = findRoom.lobby_info;
        const team0 = findRoom.team0;
        const team1 = findRoom.team1;
        let tournamentGrid = findTournament.grid

        const compliteGame = async (team) => {
          console.log(45654789, team)
          if(team == 0){
            tournamentGrid[tournamentGrid.length-1][0][0].win = true
            tournamentGrid[tournamentGrid.length-1][0][1].win = false
          }else{
            tournamentGrid[tournamentGrid.length-1][0][0].win = false
            tournamentGrid[tournamentGrid.length-1][0][1].win = true
          }
          await tournaments.updateOne({_id: findTournament._id}, {grid: tournamentGrid})
          await tournamGames.updateOne({_id:findRoom._id}, {team_victory: team, status: false})
        }
        
        function getRandomIntInclusive(min, max) {
          min = Math.ceil(min);
          max = Math.floor(max);
          return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        if(match_id != null && result_match && findRoom.userInLobby0 && findRoom.userInLobby1){

          if(findRoom.type_duel == "ONE_BY_ONE" && result_match.match_id){

            let findMember0 = _.findWhere(lobby.members, {
              team:0
            })

            let findMember1 = _.findWhere(lobby.members, {
              team:1
            })

            const createResult = async (hero, user_id, game_result, count_score, count_deaths) =>{
              const game_time = Math.floor(result_match.duration / 60) + ':' + result_match.duration % 60
              await resultDotaGames.create({
                hero, 
                user_id,
                type_game:"OneVsOne",
                game_time,
                game_result,
                count_score,
                count_deaths
              })
            }

            if(findMember0 && findMember1){

              const accountId0 = team0[0].account_id
              const accountId1 = team1[0].account_id
              let checkHero0 = undefined
              let checkHero1 = undefined

              const findPlayer0 = _.findWhere(result_match.players, {
                account_id: accountId0
              })

              const findPlayer1 = _.findWhere(result_match.players, {
                account_id: accountId1
              })

              if(findPlayer0){
                checkHero0 = _.findWhere(findRoom.random_heros, {
                  id:findPlayer0.hero_id,
                  banStatus:false
                })
              }

              if(findPlayer1){
                checkHero1 = _.findWhere(findRoom.random_heros, {
                  id:findPlayer1.hero_id,
                  banStatus:false
                })
              }

              let findAllHeros = await dotaHeros.find()

              let getHeroTeam0 = _.findWhere(findAllHeros[0].dota_heros, {
                id: findPlayer0.hero_id
              })

              let getHeroTeam1 = _.findWhere(findAllHeros[0].dota_heros, {
                id: findPlayer1.hero_id
              })

              const rangTeam0 = await levels.findOne({user_id:findRoom.team0[0]});
              const rangTeam1 = await levels.findOne({user_id:findRoom.team1[0]});

              if(checkHero0 && checkHero1 == undefined 
                || checkHero0 && result_match.radiant_score == 2 && result_match.radiant_win
                || findRoom.select_heros == "ALL PICK" && result_match.radiant_score == 2 && result_match.radiant_win
                || checkHero0 && result_match.tower_status_radiant > 2039 && result_match.radiant_win
                || findRoom.select_heros == "ALL PICK" && result_match.tower_status_radiant > 2039 && result_match.radiant_win){

                if(findRoom.userInLobby0 && findRoom.userInLobby1){
                  await dotaRating.updateMany({user_id:findRoom.team0[0]}, { $inc: { score: +1, count_games: +1 } });
                  await dotaRating.updateMany({user_id:findRoom.team1[0]}, { $inc: { count_games: +1 } });

                  await levels.updateMany({user_id:findRoom.team0[0]}, { rang:getRang(+(rangTeam0.score)+1), $inc: { score: +1 } });
                  await levels.updateMany({user_id:findRoom.team1[0]}, { rang:getRang(+(rangTeam1.score)+1), $inc: { score: +1 } });
                }

                createResult(getHeroTeam0, findRoom.team0[0], 'Победа', findPlayer0.kills, findPlayer0.deaths)
                createResult(getHeroTeam1, findRoom.team1[0], 'Поражение', findPlayer1.kills, findPlayer1.deaths)
                compliteGame(0)

              }else if(checkHero0 == undefined && checkHero1 
                || checkHero1 && result_match.dire_score == 2 && result_match.radiant_win == false
                || findRoom.select_heros == "ALL PICK" && result_match.dire_score == 2 && result_match.radiant_win == false
                || checkHero1 && result_match.tower_status_dire > 2039 && result_match.radiant_win == false
                || findRoom.select_heros == "ALL PICK" && result_match.tower_status_dire > 2039 && result_match.radiant_win == false){

                if(findRoom.userInLobby1 && findRoom.userInLobby0){
                  await dotaRating.updateMany({user_id:findRoom.team1[0]}, { $inc: { score: +1, count_games: +1 } });
                  await dotaRating.updateMany({user_id:findRoom.team0[0]}, { $inc: { count_games: +1 } });
                 
                  await levels.updateMany({user_id:findRoom.team0[0]}, { rang:getRang(+(rangTeam0.score)+1), $inc: { score: +1 } });
                  await levels.updateMany({user_id:findRoom.team1[0]}, { rang:getRang(+(rangTeam1.score)+1), $inc: { score: +1 } });
                }

                createResult(getHeroTeam1, findRoom.team1[0], 'Победа', findPlayer1.kills, findPlayer1.deaths)
                createResult(getHeroTeam0, findRoom.team0[0], 'Поражение', findPlayer0.kills, findPlayer0.deaths)
                compliteGame(1)
              }else{
               compliteGame(1)
                ctx.status = 200
                return 'Игра не по правилам!'
              }

            }else{
              if(findMember0){
                compliteGame(0)
              }else if(findMember1){
                compliteGame(1)
              }else{
                compliteGame(getRandomIntInclusive(0, 1))
              }
             
              ctx.status = 200
              return 'Игра не по правилам!'
            }
          }else if(findRoom.type_duel == "TWO_BY_TWO" && result_match.match_id){

          }else{
            ctx.status = 200
            return 'ERROR'
          }

          ctx.status = 200
          return "Ok"
        }else{

          if(findRoom.userInLobby0 && findRoom.userInLobby1 == false){
            compliteGame(0)
          }else if(findRoom.userInLobby0 == false && findRoom.userInLobby1){
            compliteGame(1)
          }else{


            compliteGame(getRandomIntInclusive(0, 1))
          }
          ctx.status = 200
          return 'StartOff'
        }      
      }else{
         console.log(212121, findRoom)
        ctx.status = 200
        return 'ERROR1'
      }
    }catch(err){
      console.log(122233, err)
      ctx.status = 200
      return "NO"
    }
  },

  registerTournament: async (ctx, next) => {
    let {tourId} = ctx.request.body;
    let { user } = ctx;
    
    try{
      let findTournament = await tournaments.findOne({_id: tourId}).deepPopulate('players players.level  players.dota_score');
      
      if(findTournament.status == "WAIT"){

        // if(findTournament.type_game == "TEAM_VS_TEAM"){

        //   let checkLeader = await teams.findOne({leader_id: user._id})
        //   if(checkLeader){
        //     if(checkLeader.activeTeam.length < 5){
        //       ctx.status = 200;
        //       return ctx.body = {
        //         message: `Количество ${checkLeader.activeTeam.length} в основном составе команды (необходимо 5)`
        //       };
        //     }
        //   }
        // }

        let findPlayer = _.findWhere(findTournament.players, {
          steamId:user.steamId
        });

        let message = '';

        if(findPlayer === undefined){
          let userBalance = await balance.findOne({user:user._id})
          if(userBalance){}{
            await balance.create({user:user._id})
            userBalance = {balance:0, coins:0}
          }

          let currencyMy = "dollar"

          if(findTournament.currency == "dollar"){
            currencyMy = userBalance.balance
          }else{
            currencyMy = userBalance.coins
          }
          let summ = +(currencyMy) - (+(findTournament.contribution));
          if (summ > -1) {
            let findTournam = await tournaments.findOne({_id: tourId});
            findTournam.players.push(user._id);

            if(findTournament.type_game == "TEAM_VS_TEAM"){
              let checkLeader = await teams.findOne({leader_id: user._id})
              if(checkLeader){
	              findTournament.teams.push(checkLeader._id)
	              await tournaments.updateOne({_id: tourId}, {teams: findTournament.teams})
              }else{
              	message = "Вам необходимо быть лидером команды"
			          ctx.status = 200;
			          return ctx.body = {
			            data:findTournament,
			            message
			          }
              }

            }

            if(findTournament.currency == "dollar"){
              await balance.updateOne({
                user:user._id
              }, {
                balance: summ
              });
            }else{
              await balance.updateOne({
                user:user._id
              }, {
                coins: summ
              });
            }

            findTournament = await tournaments.findOneAndUpdate({_id:tourId}, {players:findTournam.players}).deepPopulate('players teams players.level players.dota_score');
          } else {

            message = `Стоимость взноса $${findTournament.contribution} пополните ваш баланс`;
          }
        

          //await tournaments.updateOne({_id:tourId}, {players:[]});
          
          ctx.status = 200;
          return ctx.body = {
            data:findTournament,
            message
          }
        }
      }else{
        ctx.status = 400
        return ctx.body = "ERROR"
      }
    }catch(err){
      console.log(23, err)
      ctx.status = 500
      return ctx.body = "ERROR"
    }
  },

  loguotTournament: async (ctx, next) => {
    let {tourId} = ctx.request.body;
    let { user } = ctx;

    try{
      let findTournament = await tournaments.findOne({_id: tourId}).deepPopulate('players players.dota_score teams teams.leader_id');

      if(findTournament.status == "WAIT"){
        if(findTournament.type_game == "TEAM_VS_TEAM"){
            let findAndRemoveTeam = findTournament.teams.filter((el)=>{
              if(`${el.leader_id}` != `${user._id}`){
                return el
              }
            })
            
            await tournaments.updateOne({_id:tourId}, {teams: findAndRemoveTeam})
        }

        let findAndRemovePlayer = _.without(findTournament.players, _.findWhere(findTournament.players, {
          steamId:user.steamId
        }));

        findTournament = await tournaments.findOneAndUpdate({_id:tourId}, {players: findAndRemovePlayer}).deepPopulate('players players.level players.dota_score');
        ctx.status = 200
        return ctx.body = findTournament
      }else{
        ctx.status = 500
        return ctx.body = findTournaments
      }
    }catch(err){
      ctx.status = 500
      return ctx.body = "ERROR"
    }
  },

  getTournamGame: async (ctx) => {
    try{
      let { tourId } = ctx.params;
      let { user } = ctx;

      const tournamGame = await tournamGames.findOne({
        tournament_id: tourId,
        status: true,
        $or:[{
          'team0.0':user._id
        },{
          'team1.0':user._id
        }]
      }).deepPopulate('team0 team1 team1.level team0.level team0.dota_score team1.dota_score');
      ctx.status = 200
      return ctx.body = tournamGame
    }catch(err){
      ctx.status = 500
      return ctx.body = "ERROR"
    }
  },
  restartAllBots: async () =>{
    await bots.updateMany({}, {status:true})
  },

  updateTournamRoomsSocket: async () => {
    try{
      const tournams =  await tournaments.find({
        status: "START",
      }).deepPopulate('players players.level players.dota_score');;

      if(tournams){
        let  tournamRooms = []

        await Promise.all(tournams.map(async (item) => {
          let  tournamFindRomms = await tournamGames.find({
            tournament_id: `${item._id}`,
            status: true,
          }).deepPopulate('team0 team1 team1.level team0.level team0.dota_score team1.dota_score');

          if(tournamFindRomms && tournamFindRomms.length > 0){
            tournamRooms.push(tournamFindRomms)
          }
        }));

        return {tournams, tournamRooms}
      }else{
        return {}
      }
    }catch(err){
      console.log(8888, err)
      return {}
    }
  },

  tournamRoomSocket: async (user_id, tournam_id) => {
    try{
      return await tournamGames.findOne({
        tournament_id: tournam_id,
        status: true,
        $or:[{
          'team0.0': user_id
        },{
          'team1.0': user_id
        }]
      }).deepPopulate('team0 team1 team1.level team0.level team0.dota_score team1.dota_score');     
    }catch(err){
      console.log(12333, err)
      return {}
    }

  },
}