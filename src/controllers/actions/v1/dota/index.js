import duels from './duel.actions'
import home from './home.actions'
import dotaLauncher from './dota2.launcher.actions'
import team from './team.actions'
import tournament from './tournament.actions'
import requestInBots from './request_in_bots'

export default {
	duels,
	team,
	home,
	dotaLauncher,
	tournament,
	requestInBots
}