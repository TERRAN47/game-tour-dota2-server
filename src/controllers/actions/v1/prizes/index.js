import {
	users,
	prizes
} from '../../../../models'
import _ from 'underscore'
import fs from 'fs'
import im from 'imagemagick'
import randtoken from 'rand-token'
import axios from 'axios'

export default {
	getPrizes: async (ctx, next) => {

		let getPrizes = await prizes.find({})

		ctx.status = 200
		return ctx.body = getPrizes

	},
	sendFormPrize: async (ctx, next) => {

		let { email, firstname, phone, item } = ctx.request.body

		let text = `<section><h3>Новая заявка от ${firstname}, ${email} (${phone}).</h3></section><br/><section>Приз: ${item.title}, стоимость: ${item.price}$</section>`

		let { data } = await axios.get(`https://waylange.kz/api/mail/send/ggplay`, {
			params:{
				text,
			}
		})

		console.log(333, text, data)

		ctx.status = 200
		return ctx.body = 'Заявка отправлена'

	},
}