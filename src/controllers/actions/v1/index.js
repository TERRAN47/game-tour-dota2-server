import authActions from './auth.actions'
import docsActions from './docs.actions'
import dotaActions from './dota'
import paymentActions from './payment'
import boostActions from './boosts'
import friendActions from './friends'
import prizeActions from './prizes'
import nitificationActions from "./nitificationActions"
export {
	authActions,
	nitificationActions,
	docsActions,
	dotaActions,
	paymentActions,
	boostActions,
	friendActions,
	prizeActions
}