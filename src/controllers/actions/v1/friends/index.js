import {
	users,
	friends
} from '../../../../models'
import _ from 'underscore'
import fs from 'fs'
import im from 'imagemagick'
import randtoken from 'rand-token'

export default {
	getFriends: async (ctx, next) => {
		let { _id } = ctx.user

		let getFriends = await friends.find({
			$or:[{
				user_id:_id
			},{
				user_id_invite:_id
			}]
		}).deepPopulate('user_id user_id_invite')

		ctx.status = 200
		return ctx.body = getFriends

	},
	removeFriend: async (ctx, next) => {

		let { id } = ctx.request.body
		let { _id } = ctx.user

		await friends.find({
			_id:id,
			$or:[{
				user_id_invite:_id,
			},{
				user_id:_id
			}]
		}).remove()

		let getFriends = await friends.find({
			$or:[{
				user_id:_id
			},{
				user_id_invite:_id
			}]
		}).deepPopulate('user_id user_id_invite')

		ctx.status = 200
		return ctx.body = getFriends

	},
	invite: async (ctx, next) => {

		let {
			user_id_invite
		} = ctx.request.body

		let { _id } = ctx.user
		let user_id = _id

		if(user_id_invite){

			let findInvite = await friends.find({
				$or:[{
					user_id_invite:user_id_invite,
					user_id:user_id
				},{
					user_id_invite:user_id,
					user_id:user_id_invite
				}]
			})

			if(findInvite && findInvite.length > 0){

				ctx.status = 200
				return ctx.body = 'Пользователь уже есть в списке друзей'


			}else{

				let createInvite = await friends.create({
					user_id_invite:user_id_invite,
					user_id:user_id,
					status:true
				})

				ctx.status = 200
				return ctx.body = 'Запрос в друзья успешно добавлен'

			}

		}

	},
	search: async (ctx, next) => {

		let { q } = ctx.request.body

		if(q){

			let searchFriends = await users.find({
				'firstname': q 
			})

			ctx.status = 200
			return ctx.body = searchFriends

		}

		ctx.status = 400
		return ctx.body = {
			body:'Передайте обязательное поле'
		}

	}
}