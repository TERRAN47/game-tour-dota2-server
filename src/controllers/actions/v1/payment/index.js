import {
	users,
	transactions,
	prestige,
	balance,
	transfers
} from '../../../../models'
import moment from 'moment'
import _ from 'underscore'
import randtoken from 'rand-token'
import config from 'config'
import request from 'request'
import jwt from 'jsonwebtoken'
import md5 from 'md5'

const algorithm = 'HS512'

export default {

	checkBalance: async (ctx, next) => {

		const { user } = ctx
		const { price, days } = ctx.request.body

		let findBalanceUser = await balance.findOne({
			user:user._id
		})

		if(findBalanceUser){

			if(Number(findBalanceUser._doc.balance) >= Number(price)){
				return next()
			}

			ctx.status = 200
			return ctx.body = 'У вас не хватает средств'
			
		}

		await balance.create({
			user:user._id
		})

		ctx.status = 200
		return ctx.body = 'У вас не хватает средств'

	},

	prestigeBuy: async (ctx, next) => {

		const { user } = ctx
		const { price, days } = ctx.request.body

		let findPrestige = await prestige.findOne({
			user:user._id
		})

		await balance.update({
			user:user._id
		}, {
			$inc:{
				balance:-Number(price)
			}
		})

		if(findPrestige){

			await prestige.update({
				user:user._id
			}, {
				$inc:{
					days:Number(days)
				},
				price:Number(price)
			})

			ctx.status = 200
			return ctx.body = 'Престиж обновлен'

		}

		await prestige.create({
			user:user._id,
			price,
			days
		})

		ctx.status = 200
		return ctx.body = 'Престиж куплен'

	},

	createTransaction: async (ctx, next) => {

		let { amount } = ctx.request.body
		const { user } = ctx

		if(amount){

			let generateCode = randtoken.generate(9, "0123456789")

			try{

				const createTransaction = await transactions.create({
					user:user._id,
					amount,
					transaction_code:generateCode
				})

				//"ID Вашего магазина:Сумма платежа:Секретное слово:Номер заказа", 

				amount = Number(amount)


				ctx.userTransaction = {
					user:user._id,
					amount:amount,
					transaction_code:generateCode,
					s:md5(`159349:${amount}:ioqcqxr2:${generateCode}`)
				}

				return next()

			}catch(err){
				return ctx.body = {
					data:'error'
				}
			}

		}else{

			ctx.status = 200
			return ctx.body = 'Вернитесь и передайте параметр amount.'

		}

	},
	regirectFreeCassa: async (ctx, next) =>{

		const { userTransaction } = ctx

		ctx.status = 200
		return ctx.body = `http://www.free-kassa.ru/merchant/cash.php?m=159349&oa=${userTransaction.amount}&o=${userTransaction.transaction_code}&s=${userTransaction.s}&lang=ru&i=`


	},
	paymentProccess: async (ctx, next) => {

		let { body } = ctx.request

		let searchOrder = await transactions.findOne({
			transaction_code:body.MERCHANT_ORDER_ID,
			status:false
		})

		if(searchOrder){
			await transactions.update({
				transaction_code:body.MERCHANT_ORDER_ID,
				status:false
			},{
				status:true
			})
			return
		}

		return

	},
	paymentSuccess: async (ctx, next) => {



	},
	paymentError: async (ctx, next) => {

		
		
	},

	checkBalanceTransfer: async (ctx, next) => {

		const { user } = ctx
		const { price } = ctx.request.body

		let findBalanceUser = await balance.findOne({
			user:user._id
		})

		if(findBalanceUser){

			let procent = Number(price * 0.15).toFixed(2)
			let resultTranfer = Number(price - procent).toFixed(2)

			if(Number(findBalanceUser._doc.balance - procent) >= price){
				ctx.resultTranfer = price
				ctx.userBalance = Number(findBalanceUser._doc.balance)
				return next()
			}

			ctx.status = 200
			return ctx.body = {
				data:'У вас не хватает средств',
				balance:findBalanceUser._doc.balance
			}
			
		}

		await balance.create({
			user:user._id
		})

		ctx.status = 200
		return ctx.body = {
			data:'У вас не хватает средств',
			balance:findBalanceUser._doc.balance
		}

	},

	transferCreate: async (ctx, next) => {

		//transfers
		const { user, resultTranfer, userBalance } = ctx
		const { 
			price,
			formPayment,
			service
		} = ctx.request.body

		try{

			let procent = Number(resultTranfer * 0.15).toFixed(2)
			let procentAdmin = Number(resultTranfer - procent).toFixed(2)

			let createTransfer = await transfers.create({
				user_id:user._id,
				type_payment:service,
				value_payment:formPayment,
				tranfer:Number(resultTranfer),
				procent:procentAdmin
			})

			await balance.update({
				user:user._id
			}, {
				$inc:{
					balance:-Number(resultTranfer)
				}
			})

			let getUserBalance = await balance.findOne({
				user:user._id
			})

			ctx.status = 200
			return ctx.body = {
				data:'Заявка успешно подана',
				balance:getUserBalance._doc.balance
			}

		}catch(err){
			console.log(333, err)
			ctx.status = 500
			return ctx.body = 'Error'
		}

	}

}