import {
	users,
	prestige,
	balance
} from '../../../models'
import moment from 'moment'
import _ from 'underscore'
import randtoken from 'rand-token'
import config from 'config'
import request from 'request'
import jwt from 'jsonwebtoken'

const algorithm = 'HS512'

export default {
	getUser: async (ctx, next) => {

		let { user } = ctx

		let getBalance = await balance.findOne({
			user:user._id
		})

		let getPrestige = await prestige.findOne({
			user:user._id
		})

		ctx.body = 200
		return ctx.body = {
			...user,
			balance:getBalance ? getBalance._doc.balance : null,
			coins:getBalance ? getBalance._doc.coins : null,
			prestige:getPrestige ? true : null,
		}
	},

	getProfileInfo: async (ctx, next) => {

		let { user } = ctx

		let getBalance = await balance.findOne({
			user:user._id
		})

		let getPrestige = await prestige.findOne({
			user:user._id
		})

		ctx.body = 200
		return ctx.body = {
			...user,
			balance:getBalance ? getBalance._doc.balance : null,
			coins:getBalance ? getBalance._doc.coins : null,
			prestige:getPrestige ? true : null,
		}
	},

	returnTokenUser: async (ctx, next) => {

	  	const token = jwt.sign({
	  		user: ctx.state.user 
	  	}, config.secret, { 
	  		expiresIn: '1 days' 
	  	})

	  	return ctx.render('api/auth/steam', {
	  		token
	  	})
	},

	logoutUser: async (ctx, next) => {

	  	ctx.logout()

	  	return ctx.body = ctx.isAuthenticated()
	}	
}