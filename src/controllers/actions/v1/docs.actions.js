
import { navList } from '../../../domain'

export default {
	home: async (ctx, next) => {

		return ctx.render('api/docs/v1/home', {
			title:'Documentation api v1',
			navList
		})

	}
}