import {
	users,
	ips
} from '../../models'

import { RateLimiterMemory } from 'rate-limiter-flexible'

const rateLimiter = new RateLimiterMemory({
    points: 5, // 5 points
    duration: 1, // per second
})

export default {
	home: async (ctx, next) => {

		return ctx.render('app/index', {
			title: 'GameTour'
		})

	},
	admin: async (ctx, next) => {
		return ctx.render('admin/index', {
			title: 'GameTour'
		})
	},
	insertIP: async (ctx, next) => {

		let { ip } = ctx
		ip = ip.replace('::ffff:','');

		const findIP = await ips.findOne({
			ip
		})

		if(findIP){

			await ips.update({
				ip
			},{
				requests:(findIP.requests + 1)
			})

			return next()

		}else{

			await ips.create({
				ip
			})

			return next()

		}

	},
	limitRequests: async (ctx, next) => {
	  try {
	    await rateLimiter.consume(ctx.ip)
	    return next()
	  } catch (rejRes) {

	    ctx.status = 429
	    return ctx.body = 'Too Many Requests'
	  }
	},
	acceptCert: async (ctx, next) => {

		let { id } = ctx.params

		ctx.type = 'text/plain; charset=utf-8'
		ctx.status = 200
		return ctx.body = id

	}
}