import Router from 'koa-router'

//routes
import mainRoutes from './routes/main.routes'
import routesApiV1 from './routes/v1'
import routesAdminV1 from './routes/admin'

const router = new Router()

router.use(routesApiV1)
router.use(mainRoutes)
router.use(routesAdminV1)

export default router.routes()