import IO from 'koa-socket-2'
import fs from 'fs'
import config from 'config'

import main from './main'
import chat from './chat'
import users from './users'
import dota from './dota'
const PORT = config.get('DEFAULT_PORT')

const io = new IO()
const chatSockets = new IO('chat')
const userSockets = new IO('users')
const dotaSockets = new IO('dota')

const sockets = async (app) => {

	//settings
	io.attach(app)
	chatSockets.attach(app)
	userSockets.attach(app)
	dotaSockets.attach(app)

	//sockets chanells
	main(io)
	chat(chatSockets)
	users(userSockets)
	dota(dotaSockets)

	console.log('started socket server')

	//port
	app.listen(3187)
	//io.emit('an event sent to all connected clients');
}

export default sockets