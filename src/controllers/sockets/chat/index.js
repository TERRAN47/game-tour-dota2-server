
import { authMiddleware } from '../middlewares'


const chat = async (io) => {

	await authMiddleware(io)

	io.on('connection', (ctx, data) => {
	  	console.log('connection')

	})

	io.on('disconnect', (ctx, data) => {
	  console.log('disconnect' )
	})

	io.on('infoUser', (ctx, data) => {

		ctx.socket.broadcast.emit('infoUser', { 
			user:io.socket.user
		})

	})

	io.on('message', (ctx, data) => {
	  
		ctx.socket.join(data.room)

		io.to(data.room).emit( 'message', { 
			text:data.text
		})

	})	

}


export default chat