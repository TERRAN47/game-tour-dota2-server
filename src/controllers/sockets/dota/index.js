import { authMiddleware } from '../middlewares'
import { dotaActions } from '../../actions/v1'
import cron from 'node-cron';
let dotaSockets = {}


const dota = async (io) => {

	await authMiddleware(io)

	io.on('connection', (ctx, data) => {

		dotaSockets[io.socket.user._id] = ctx.id

  	console.log(115, dotaSockets);
	})

	io.on('disconnect', (ctx, data) => {

  		console.log('disconnect', data);
	})

	io.on('startTournament', async (ctx, data) => {

		const tournamActive = await dotaActions.tournament.startTournamSocket()
		io.emit('startTournament', { 
			tournamActive
		});
	});


	io.on('tournamJoinRoom', async (ctx, data) => {
		ctx.socket.join(`${data.tournam_id}`)

		let tournamRoom = await dotaActions.tournament.tournamRoomSocket(`${io.socket.user._id}`, data.tournam_id);

		if(tournamRoom){
			ctx.socket.emit('tournamRoom', { 
				tournamRoom
			});
		}
	});

	//cron.schedule('*/1 * * * *', async () => {
	// 	let {tournams, tournamRooms} = await dotaActions.tournament.updateTournamRoomsSocket();

	// 	if(tournams && tournamRooms && tournamRooms.length > 0 && tournams.length > 0){
	// 		tournams.forEach((item, index)=>{
	// 			if(tournamRooms[index]){
	// 				io.to(`${item._id}`).emit('tournamRooms', { 
	// 					tournamRooms: tournamRooms[index],
	// 					tournamentActive: item
	// 				});
	// 			}
	// 		})
	// 	}
	// });

	// setInterval(async ()=>{

	// }, 60000)
}

export default dota