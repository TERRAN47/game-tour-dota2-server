import jwt from 'jsonwebtoken'
import config from 'config'

const authMiddleware = async (io) => {

	io.socket.use(async (socket, next) => {
	  try {
	    let query = socket.handshake.query;

	    if(!query.authorization) {

	    	throw new Error('No authorization header specified.')

	    }

		await jwt.verify(query.authorization, config.secret, async (err, decoded) => {

		  	if(err){
			  	throw new Error('Token is not valited.')
		  	}else{

			    io.socket.user = decoded.user
			    //console.log(12345, io.socket.user)
			    next()

		  	}
		})

	  } catch(err) {
	    console.log(err.message)
	    next(err)
	    socket.disconnect()
	  }
	})

}

export default authMiddleware