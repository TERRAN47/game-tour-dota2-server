import Router from 'koa-router'
import mainActions from '../actions/main.actions'
import qt from 'quickthumb'
import path from 'path'

const router = new Router()

router.get('/', mainActions.home)
//router.get('/admin', mainActions.admin)
//router.all('/.well-known/acme-challenge/:id', mainActions.acceptCert)
/*router.get('/images', async (ctx, next) => {
	return qt.static(path.join(__dirname, '../../../public/uploads/images/'))
})
*/

router.allowedMethods()

export default router.routes()