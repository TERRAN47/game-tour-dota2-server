import Router from 'koa-router'

import authRoutes from './auth.routes'
import tournamentRoutes from './tournament.routes'
import userRoutes from './user.routes'
import transactionsRoutes from './transaction.routes'
import applicationRoutes from './application.routes'
import prizeRoutes from './prize.routes'

const router = new Router({
	prefix: '/admin/v1'
})

router.use(authRoutes)
router.use(tournamentRoutes)
router.use(userRoutes)
router.use(transactionsRoutes)
router.use(applicationRoutes)
router.use(prizeRoutes)

export default router.routes()