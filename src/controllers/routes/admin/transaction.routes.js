import Router from 'koa-router'
import passport from 'koa-passport'
import config from 'config'
import { transactionActions } from '../../actions/admin'
import { fieldsMiddleware, authMiddleware } from '../../../middlewares'
import jwt from 'jsonwebtoken'

const router = new Router()

router.get('/transactions', authMiddleware.authentication, authMiddleware.isAdmin, transactionActions.getTransactions)
router.get('/transactions/test/create', transactionActions.createTransaction)

router.get('/transfers', authMiddleware.authentication, authMiddleware.isAdmin, transactionActions.getTransfers)
router.post('/transfers/delete', authMiddleware.authentication, authMiddleware.isAdmin, transactionActions.removeTransfer)

export default router.routes()