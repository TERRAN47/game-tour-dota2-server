import Router from 'koa-router'
import passport from 'koa-passport'
import config from 'config'
import { authActions } from '../../actions/admin'
import { fieldsMiddleware, authMiddleware } from '../../../middlewares'
import jwt from 'jsonwebtoken'

const router = new Router()

router.post('/authentication', authActions.authentication, authMiddleware.isAdmin, authActions.finalAuthentication)


export default router.routes()