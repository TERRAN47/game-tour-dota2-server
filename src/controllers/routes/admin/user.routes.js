import Router from 'koa-router'
import passport from 'koa-passport'
import config from 'config'
import { userActions } from '../../actions/admin'
import { fieldsMiddleware, authMiddleware } from '../../../middlewares'
import jwt from 'jsonwebtoken'

const router = new Router()

router.get('/users', authMiddleware.authentication, authMiddleware.isAdmin, userActions.getUsers)
router.post('/users/ban', authMiddleware.authentication, authMiddleware.isAdmin, userActions.banUser)
router.post('/users/remove', authMiddleware.authentication, authMiddleware.isAdmin, userActions.removeUser)
router.post('/user/change/balance', authMiddleware.authentication, authMiddleware.isAdmin, userActions.userCheckBalance, userActions.userChangeBalance)

export default router.routes()