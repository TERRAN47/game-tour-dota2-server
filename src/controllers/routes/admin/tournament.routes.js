import Router from 'koa-router'
import passport from 'koa-passport'
import config from 'config'
import { tournamentActions } from '../../actions/admin'
import { 
	fieldsMiddleware, 
	authMiddleware,
	filesMiddleware
} from '../../../middlewares'
import jwt from 'jsonwebtoken'

const router = new Router()

router.get('/tournaments', authMiddleware.authentication, authMiddleware.isAdmin, tournamentActions.getTournaments)
router.post('/tournaments/create', authMiddleware.authentication, authMiddleware.isAdmin, filesMiddleware.checkSizeFile, filesMiddleware.saveImage, tournamentActions.createTournament)
router.get('/tournament/view', authMiddleware.authentication, authMiddleware.isAdmin, tournamentActions.getTournament)
router.post('/tournament/start', authMiddleware.authentication, authMiddleware.isAdmin, tournamentActions.startTournament)
router.post('/tournaments/save-rules', authMiddleware.authentication, authMiddleware.isAdmin, tournamentActions.tournamentSaveRules)

export default router.routes()