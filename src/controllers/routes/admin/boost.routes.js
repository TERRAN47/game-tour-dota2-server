import Router from 'koa-router'
import passport from 'koa-passport'
import config from 'config'
import { boostActions } from '../../actions/admin'
import { fieldsMiddleware, authMiddleware } from '../../../middlewares'
import jwt from 'jsonwebtoken'

const router = new Router()

router.get('/boosts', authMiddleware.authentication, authMiddleware.isAdmin, boostActions.getBoosts)

export default router.routes()