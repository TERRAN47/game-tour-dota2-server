import Router from 'koa-router'
import passport from 'koa-passport'
import config from 'config'
import { prizeActions } from '../../actions/admin'
import {
	fieldsMiddleware, 
	authMiddleware, 
	filesMiddleware 
} from '../../../middlewares'
import jwt from 'jsonwebtoken'

const router = new Router()

router.get('/prizes', authMiddleware.authentication, authMiddleware.isAdmin, prizeActions.getPrizes)
router.post('/prizes/remove', authMiddleware.authentication, authMiddleware.isAdmin, prizeActions.removePrize)
router.post('/prizes/create', authMiddleware.authentication, authMiddleware.isAdmin, filesMiddleware.checkSizeFile, filesMiddleware.saveImagePrize, prizeActions.create)

export default router.routes()