import Router from 'koa-router'
import passport from 'koa-passport'
import config from 'config'
import { applicationActions } from '../../actions/admin'
import { fieldsMiddleware, authMiddleware } from '../../../middlewares'
import jwt from 'jsonwebtoken'

const router = new Router()

router.get('/applications', authMiddleware.authentication, authMiddleware.isAdmin, applicationActions.GET)

export default router.routes()