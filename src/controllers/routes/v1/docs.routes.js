import Router from 'koa-router'
import { docsActions } from '../../actions/v1'

const router = new Router()

router.get('/docs', docsActions.home)

export default router.routes()