import Router from 'koa-router'
import passport from 'koa-passport'
import config from 'config'
import { authActions } from '../../actions/v1'
import { fieldsMiddleware, authMiddleware } from '../../../middlewares'
import jwt from 'jsonwebtoken'

const router = new Router()

router.get('/auth/user', authMiddleware.authentication, authActions.getUser)
router.get('/auth/profile-info', authMiddleware.authentication, authActions.getProfileInfo)

router.get('/auth/steam', passport.authenticate('steam'))

router.get('/auth/steam/return', passport.authenticate('steam'), authMiddleware.isBan, authActions.returnTokenUser)

router.get('/auth/steam/logout', authActions.logoutUser)



export default router.routes()