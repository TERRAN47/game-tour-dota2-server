import Router from 'koa-router'
import passport from 'koa-passport'
import config from 'config'
import { prizeActions } from '../../../actions/v1'
import { fieldsMiddleware, authMiddleware } from '../../../../middlewares'


const router = new Router()

router.get('/prizes', authMiddleware.authentication, prizeActions.getPrizes)
router.post('/prizes/send', authMiddleware.authentication, prizeActions.sendFormPrize)

export default router.routes()