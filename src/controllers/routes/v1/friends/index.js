import Router from 'koa-router'
import passport from 'koa-passport'
import config from 'config'
import { friendActions } from '../../../actions/v1'
import { fieldsMiddleware, authMiddleware } from '../../../../middlewares'


const router = new Router()

router.get('/friends', authMiddleware.authentication, friendActions.getFriends)
router.post('/friends/invite', authMiddleware.authentication, friendActions.invite)
router.post('/friends/search', authMiddleware.authentication, friendActions.search)
router.post('/friends/remove', authMiddleware.authentication, friendActions.removeFriend)

export default router.routes()