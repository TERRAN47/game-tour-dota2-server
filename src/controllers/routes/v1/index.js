import Router from 'koa-router'

import authRoutes from './auth.routes'
import docsRoutes from './docs.routes'
import dotaRoutes from './dota'
import paymentRoutes from './payment'
import boostRoutes from './boosts'
import friendRoutes from './friends'
import prizesRoutes from './Prizes'
import notifications from "./notifications"

const router = new Router({
	prefix: '/v1'
})

router.use(authRoutes)
router.use(paymentRoutes)
router.use(boostRoutes)
router.use(friendRoutes)
router.use(prizesRoutes)
router.use(dotaRoutes)
router.use(docsRoutes)
router.use(notifications)
export default router.routes()