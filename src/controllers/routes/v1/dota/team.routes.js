import Router from 'koa-router'
import passport from 'koa-passport'
import config from 'config'
import { dotaActions } from '../../../actions/v1'
import { 
	fieldsMiddleware, 
	authMiddleware,
	filesMiddleware
} from '../../../../middlewares'

const router = new Router()

router.post('/create/dota-team', authMiddleware.authentication, filesMiddleware.checkSizeFile, filesMiddleware.saveImage, dotaActions.team.createTeam)
router.get('/get/dota-team', authMiddleware.authentication, dotaActions.team.getTeam)
router.get('/remov/dota-team', authMiddleware.authentication, dotaActions.team.removeTeam)
router.get('/out/dota-team', authMiddleware.authentication, dotaActions.team.outTeam)
router.post('/user-switch/dota-team', authMiddleware.authentication, dotaActions.team.swithcUser)

export default router.routes()

