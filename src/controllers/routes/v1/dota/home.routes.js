import Router from 'koa-router'
import passport from 'koa-passport'
import config from 'config'
import { dotaActions } from '../../../actions/v1'
import { fieldsMiddleware, authMiddleware } from '../../../../middlewares'


const router = new Router()

router.get('/rating-top10', authMiddleware.authentication, dotaActions.home.getAllRating)
router.get('/get/profile/:id', authMiddleware.authentication, dotaActions.home.getProfile)


export default router.routes()