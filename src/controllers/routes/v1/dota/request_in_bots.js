
import Router from 'koa-router'
import passport from 'koa-passport'
import config from 'config'
import { dotaActions } from '../../../actions/v1'
import { fieldsMiddleware, authMiddleware } from '../../../../middlewares'


const router = new Router()

router.post('/update/user_account_id', authMiddleware.checkBot,  dotaActions.requestInBots.updUserAccountId)
router.post('/update/tournamNewLobby', authMiddleware.checkBot,  dotaActions.requestInBots.tournamNewLobby)
router.post('/update/updateOneBot', authMiddleware.checkBot,  dotaActions.requestInBots.updateOneBot)


export default router.routes()