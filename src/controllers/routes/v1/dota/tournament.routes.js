import Router from 'koa-router'
import passport from 'koa-passport'
import config from 'config'
import { dotaActions } from '../../../actions/v1'
import { authMiddleware} from '../../../../middlewares'

const router = new Router()
router.get('/get/tournament', 
	authMiddleware.authentication,
	dotaActions.tournament.getTournament
)
router.get('/get/room-game/:tournam_id', 
	authMiddleware.authentication,
	dotaActions.tournament.getRoomGame
)

router.get('/run/tournament/:time', 
	dotaActions.tournament.checkComplitGames  //startTournament //checkComplitGames
)

router.get('/tournamentRoom/:tourId',
	authMiddleware.authentication,
	dotaActions.tournament.getTournamGame
)

router.post('/regis/tournament', 
	authMiddleware.authentication,
	dotaActions.tournament.registerTournament
)

router.post('/logout/tournament', 
	authMiddleware.authentication,
	dotaActions.tournament.loguotTournament
)
export default router.routes()