 import Router from 'koa-router'
import { dotaActions } from '../../../actions/v1'
import { authMiddleware, dotaMiddeleware } from '../../../../middlewares'

const router = new Router()

router.get('/update/duel-room/:room_id', authMiddleware.authentication, dotaActions.duels.updateDuelRoom)
router.get('/get-duels-rooms', authMiddleware.authentication, dotaActions.duels.getDuelsRooms)
router.get('/delete/duel-room/:room_id', authMiddleware.authentication, dotaActions.duels.deleteDuelRoom)
router.get('/leave/duel-room/:room_id', authMiddleware.authentication, dotaActions.duels.leaveDuelRoom)
router.get('/check/slot/:room_id', authMiddleware.authentication, dotaActions.duels.checkSlotRoom)
router.get('/start/ready/:room_id', authMiddleware.authentication, dotaActions.duels.startReady)
router.get('/random/heros/:room_id', authMiddleware.authentication, dotaActions.duels.getRandomHeros)
router.get('/switch/slot/:slot_id', authMiddleware.authentication, dotaActions.duels.switchSlot)
//router.get('/result/match/:room_id', authMiddleware.authentication, dotaActions.duels.resultMatch)

router.post('/check/free-loby', authMiddleware.authentication, dotaMiddeleware.checkDuelData, dotaActions.duels.checkFreeBot)
router.post('/create/duel-room', authMiddleware.authentication, dotaActions.duels.createDuelRoom)
router.post('/ban/hero', authMiddleware.authentication, dotaActions.duels.banHero)
router.post('/kick/gamer', authMiddleware.authentication, dotaActions.duels.kickGamer)

export default router.routes()