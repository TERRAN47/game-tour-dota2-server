import Router from 'koa-router'
import { 
	dotaActions
} from '../../../actions/v1'
import { 
	authMiddleware
} from '../../../../middlewares'


const router = new Router()

router.post('/dota-launcher/create-lobby',  dotaActions.dotaLauncher.createOneVsOneLobby)
router.get('/result/match/:room_id', authMiddleware.authentication, dotaActions.dotaLauncher.resultMatch)
router.post('/two-vs-two/create-lobby',  dotaActions.dotaLauncher.createTwoVsTwoLobby)

router.get('/invite/to/lobby/:idLobby', dotaActions.dotaLauncher.inviteToLobby)

//router.post('/create/bot', dotaActions.dotaLauncher.createBot)
export default router.routes()