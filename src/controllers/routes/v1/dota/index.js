import Router from 'koa-router'

import duelRoutes from './duel.routes'
import homeRoutes from './home.routes'
import teamRoutes from './team.routes'
import dotaLauncherRoutes from './dota2.game.launcher'
import tournamentRoutes from './tournament.routes'
import requestInBots from "./request_in_bots"

const router = new Router({
	prefix: '/dota'
})

router.use(homeRoutes)
router.use(requestInBots)
router.use(duelRoutes)
router.use(teamRoutes)
router.use(dotaLauncherRoutes)
router.use(tournamentRoutes)

export default router.routes()