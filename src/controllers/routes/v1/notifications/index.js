
import Router from 'koa-router'
import passport from 'koa-passport'
import config from 'config'
import { nitificationActions } from '../../../actions/v1'
import { fieldsMiddleware, authMiddleware } from '../../../../middlewares'


const router = new Router()

router.get('/get/notifications', authMiddleware.authentication, nitificationActions.getNitifications)
router.post('/ivite/user/inteam', authMiddleware.authentication, nitificationActions.sendTeamNitify)

router.get('/not/aproov/team/:team_id', authMiddleware.authentication, nitificationActions.notAproovTeam)
router.get('/aproov/team/:team_id', authMiddleware.authentication, nitificationActions.aproovTeam)

export default router.routes()