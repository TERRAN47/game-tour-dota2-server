import Router from 'koa-router'
import config from 'config'
import { paymentActions } from '../../../actions/v1'
import { fieldsMiddleware, authMiddleware } from '../../../../middlewares'

const router = new Router()

router.post('/payments/create-transaction', authMiddleware.authentication, paymentActions.createTransaction, paymentActions.regirectFreeCassa)
router.post('/payments/proccess', paymentActions.paymentProccess)
router.get('/payments/success', paymentActions.paymentSuccess)
router.get('/payments/error', paymentActions.paymentError)

//prestige
router.post('/prestige/buy', authMiddleware.authentication, paymentActions.checkBalance, paymentActions.prestigeBuy)
router.post('/transfer/create', authMiddleware.authentication, paymentActions.checkBalanceTransfer, paymentActions.transferCreate)

export default router.routes()