import Router from 'koa-router'
import passport from 'koa-passport'
import config from 'config'
import { boostActions } from '../../../actions/v1'
import { fieldsMiddleware, authMiddleware } from '../../../../middlewares'


const router = new Router()

router.post('/boosts/create', authMiddleware.authentication, boostActions.createBoost)


export default router.routes()