import config from 'config';
import http from 'http';
import https from 'https';
import fs from 'fs';
import app from './app';
import sockets from './controllers/sockets'

const PORT = config.get('DEFAULT_PORT');

var options = {
  	key: fs.readFileSync('/etc/letsencrypt/live/ggameplay.net/privkey.pem'),
  	cert: fs.readFileSync('/etc/letsencrypt/live/ggameplay.net/cert.pem')
};

sockets(app)

https.createServer(options, app.callback()).listen(443, () => {
	console.log(`started default server on port ${443}`)
});

// http.createServer(app.callback()).listen(PORT, '0.0.0.0', () => {
// 	console.log(`started default server on port ${PORT}`)
// });

