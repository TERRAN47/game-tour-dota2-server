import Koa from 'koa';
import logger from 'koa-logger';
import protect from 'koa-protect';
import json from 'koa-json';
import koaBody from 'koa-body';
import bodyParser from 'koa-bodyparser';
import koaNunjucks from 'koa-nunjucks-2';
import session from 'koa-session';
import config from 'config';
import routes from './controllers/index';
import serve from 'koa-static';
import path from 'path';
import ip from 'koa-ip';
import morgan from 'koa-morgan';
import fs from 'fs';
import mainActions from './controllers/actions/main.actions';
import redis from 'redis';
import cors from 'koa2-cors';
import spaStatic from 'koa-spa-static';
import passport from 'koa-passport';

require('./utils/passport/index')

import {
  blacksListIP,
  whitesListIP
} from './domain'

const app = new Koa()
const accessLogStream = fs.createWriteStream(__dirname + '/access.log', { flags: 'a' })


app.use(koaBody({
	multipart: true 
}))

app.use(json())
app.use(bodyParser())

app.use(koaNunjucks({
  	ext: 'html',
  	path: path.join(__dirname, 'views'),
  	nunjucksConfig: {
    	trimBlocks: true,
    	watch: true
  	}
}))

app.use(serve(path.join(__dirname, '../public/'), {
  dotfiles: 'allow',
  hidden: true
}))

app.use(morgan('combined', { stream: accessLogStream }))
app.use(logger())

app.use(protect.koa.sqlInjection({  
    body: true,  
    loggerFunction: console.error  
}))  
  
app.use(protect.koa.xss({  
    body: true,  
    loggerFunction: console.error  
})) 

app.use(cors())

app.use(mainActions.insertIP)

app.use(ip({
  whitelist: whitesListIP,
  blacklist: blacksListIP(),
  handler: async (ctx) => {
    ctx.status = 403
  }
}))

app.use(passport.initialize())
app.use(routes)

export default app