import mongoose from '../../connectors/mongo'
import deepPopulate from 'mongoose-deep-populate'

const transferSchema = new mongoose.Schema({
  	user_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user'
  	},
    type_payment:{
      type: String  
    },
    value_payment:{
      type: String  
    },
    tranfer:{
      type: String  
    },
    status:{
      type: Boolean,
      default:false
    },
    procent:{
      type: String
    }
},{
  timestamps: true
})

transferSchema.plugin(deepPopulate(mongoose))
const transfers = mongoose.model('transfers', transferSchema)

export default transfers