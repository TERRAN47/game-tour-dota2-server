import mongoose from '../../connectors/mongo'
import deepPopulate from 'mongoose-deep-populate'

const duel_dotaSchema = new mongoose.Schema({
  hoster_id:{
    type:mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
	team0: [{
		type:mongoose.Schema.Types.ObjectId,
		ref: 'user'
	}],
  team1: [{
    type:mongoose.Schema.Types.ObjectId,
    ref: 'user'
  }],
  count_create_loby: {
    type: Number,
    default:0
  },
  bot_id:{
    type:mongoose.Schema.Types.ObjectId,
    default:null,
    ref: 'bots'
  },
	bet: {
		type: Number,
		default:0
	},
  type_game: {
    type: String,
    default:'SINGLE' // ALL PICK
  },
  type_duel:{
    type: String,
    default:'ONE_BY_ONE' // TWO_BY_TWO OR ONE_BY_ONE
  },
  currency:{
    type: String,
    default:'DOLLARS' // DOLLARS OR COINS
  },
  status:{
    type: Boolean,
    default:true
  },
  match_id:{
    type: Number,
    default:null
  },
  lobby_id:{
    type: String,
    default:''
  },
  lobby_login:{
    type: JSON,
    default:{}
  },
  lobby_info:{
    type: JSON,
    default:{}
  },
  team0_ready:{
    type: Array,
    default:[]
  },
  team1_ready:{
    type: Array,
    default:[]
  },
  random_heros:{
    type: Array,
    default:[]    
  }
},{
  timestamps: true
})

duel_dotaSchema.plugin(deepPopulate(mongoose))
const duelsDota = mongoose.model('duels_dota', duel_dotaSchema)

export default duelsDota