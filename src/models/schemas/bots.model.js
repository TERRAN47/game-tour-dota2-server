import mongoose from '../../connectors/mongo'
import deepPopulate from 'mongoose-deep-populate'
const botsSchema = new mongoose.Schema({
	bot_name:{
    type: String,
    default:''
  },
  login:{
    type: String,
    default:''
  },
  password:{
    type: String,
    default:''
  },
  room_id:{
    type:String,
    default:'',
  },

  league_id:{
    type: Number,
    default:0,
  },
  port:{
    type: String,
    default:''
  },
  status:{
    type: Boolean,
    default:true
  },
  bot_type:{
    type: String,
    default:'duel' // duel , tournament
  }
},{
  timestamps: true
})
botsSchema.plugin(deepPopulate(mongoose))
const bots = mongoose.model('bots',botsSchema)
export default bots