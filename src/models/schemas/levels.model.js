import mongoose from '../../connectors/mongo'
import deepPopulate from 'mongoose-deep-populate'

const levelSchema = new mongoose.Schema({
  	rang: {
  		type: Number,
  		default:1
  	},
  	score: {
  		type: Number,
  		default:0
  	},
  	user_id:{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user'
  	}
})

levelSchema.plugin(deepPopulate(mongoose))
const levels = mongoose.model('levels', levelSchema)

export default levels