import mongoose from '../../connectors/mongo'
import deepPopulate from 'mongoose-deep-populate'

const ratingDotaSchema = new mongoose.Schema({
    score: {
      type: Number,
      default:0
    },
    count_games:{
      type: Number,
      default:0
    },
    user_id:{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user'
    }
})

ratingDotaSchema.plugin(deepPopulate(mongoose))
const dotaRating = mongoose.model('rating_dota', ratingDotaSchema)

export default dotaRating