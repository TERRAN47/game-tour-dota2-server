import mongoose from '../../connectors/mongo'
import deepPopulate from 'mongoose-deep-populate'

const prestigeSchema = new mongoose.Schema({
  user:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  days:{
    type:Number,
    default:0
  },
  price: {
    type:Number,
    default:0 
  }
})

prestigeSchema.plugin(deepPopulate(mongoose))
const prestige = mongoose.model('prestige', prestigeSchema)

export default prestige