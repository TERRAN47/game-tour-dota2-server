import mongoose from '../../connectors/mongo'
import deepPopulate from 'mongoose-deep-populate'

const notifyTeamSchema = new mongoose.Schema({
  team: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'teams'
  },
  new_status: {
    type: Boolean,
    default: true
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  }
})

notifyTeamSchema.plugin(deepPopulate(mongoose))
const notifyTeams = mongoose.model('notifyTeams', notifyTeamSchema)

export default notifyTeams