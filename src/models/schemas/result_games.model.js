import mongoose from '../../connectors/mongo'

const resultGamesSchema = new mongoose.Schema({
	team_id: {
		type: String
	},
	game_result: {
		type: String
	},
	game_time:{
    	type: String
	},
	count_wins: {
		type: String
	},
	count_losing:{
    	type: String
	},
	tournament_name:{
    	type: String
	},
})

const resultGames = mongoose.model('result_games', resultGamesSchema)

export default resultGames