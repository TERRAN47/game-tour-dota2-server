import mongoose from '../../connectors/mongo'
import deepPopulate from 'mongoose-deep-populate'

const frinedSchema = new mongoose.Schema({
  	status: {
  		type: Boolean,
  		default:false
  	},
    user_id_invite:{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user'
    },
  	user_id:{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user'
  	}
})

frinedSchema.plugin(deepPopulate(mongoose))
const friends = mongoose.model('frineds', frinedSchema)

export default friends