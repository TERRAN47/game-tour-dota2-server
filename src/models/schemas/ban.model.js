import mongoose from '../../connectors/mongo'

const banSchema = new mongoose.Schema({
  	user_id: {
  		type: String
  	},
    status_ban:{
      type: Boolean,
      default:false  
    }
},{
  timestamps: true
})

const bans = mongoose.model('bans', banSchema)

export default bans