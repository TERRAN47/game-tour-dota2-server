import mongoose from '../../connectors/mongo'

const boostSchema = new mongoose.Schema({
  	user_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user'
  	},
    type_boost:{
      type: String,
      default:''   
    },
    skill_point:{
      type: String,
      default:'0'   
    },
    price:{
      type: String,
      default:'' 	
    },
    vk:{
      type: String,
      default:''  	
    },
    email:{
      type: String,
      default:''  	
    },
    games:{
      type: String,
      default:'1'  	
    },
    previous_level:{
      type: String,
      default:'1'  	
    },
    previous_rang:{
      type: String,
      default:'1'  	
    },
    previous_point_mmr:{
      type: String,
      default:'1'  	
    },
    login_steam:{
      type: String,
      default:''  	
    },
    password_steam:{
      type: String,
      default:''  	
    },
    phone:{
      type: String,
      default:'' 	
  	},
    real_point_mmr:{
      type: String,
      default:'0' 	
  	},
    boost_party_mmr:{
      type: Boolean,
      default:false
  	},
    to_run_faster:{
      type: Boolean,
      default:false
  	},
    dota_play_on_my_heroes:{
      type: Boolean,
      default:false
  	},
    do_not_play_at_certain_times:{
      type: Boolean,
      default:false
  	},
    wishes:{
      type: String,
      default:''  
    },
    mmr:{
      type: String,
      default:'0'  
    },
    days:{
      type: String,
      default:'0'  
    },
    training:{
      type: String,
      default:'0'  
    }
},{
  timestamps: true
})

const boosts = mongoose.model('boosts', boostSchema)

export default boosts