import mongoose from '../../connectors/mongo'
import deepPopulate from 'mongoose-deep-populate'

const ratingCsgoSchema = new mongoose.Schema({
    score: {
      type: Number,
      default:0
    },
    count_games:{
      type: Number,
      default:0
    },
    user_id:{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user'
    }
})

ratingCsgoSchema.plugin(deepPopulate(mongoose))
const csgoRating = mongoose.model('rating_csgo', ratingCsgoSchema)

export default csgoRating