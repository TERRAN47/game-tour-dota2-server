import mongoose from '../../connectors/mongo'

const roleSchema = new mongoose.Schema({
  	user_id: {
  		type: String
  	},
    role:{
      type: String,
      default:'user'   
    }
},{
  timestamps: true
})

const roles = mongoose.model('roles', roleSchema)

export default roles