import mongoose from '../../connectors/mongo'

const ipSchema = new mongoose.Schema({
  	ip: {
  		type: String,
      default:'0.0.0.0'
  	},
  	requests: {
  		type: Number,
  		default:1
  	},
  	status: {
  		type: Boolean,
      default:true
  	}
})

const ips = mongoose.model('ip', ipSchema)

export default ips