import mongoose from '../../connectors/mongo'

const resultDotaGamesSchema = new mongoose.Schema({
	hero: {
		type: Object
	},
	user_id:{
		type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
	},
	type_game: {
		type: String
	},
	game_time:{
  	type: String
	},
	game_result: {
		type: String
	},
	count_score:{
  	type: Number
	},
	count_deaths:{
  	type: Number
	},
})

const resultDotaGames = mongoose.model('result_dota_games', resultDotaGamesSchema)

export default resultDotaGames