import mongoose from '../../connectors/mongo'
import deepPopulate from 'mongoose-deep-populate'

const balanceSchema = new mongoose.Schema({
  	user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user'
  	},
  	coins:{
      type: Number,
      default:0
    },
    balance:{
      type: Number,
      default:0
    }
})

balanceSchema.plugin(deepPopulate(mongoose))
const balance = mongoose.model('balance', balanceSchema)

export default balance