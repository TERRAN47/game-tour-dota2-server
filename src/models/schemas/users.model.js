import mongoose from '../../connectors/mongo'
import deepPopulate from 'mongoose-deep-populate'

const userSchema = new mongoose.Schema({
  	firstname: {
  		type: String,
  		default:'anonim'
  	},
  	steamId: {
  		type: String,
  		default:''
  	},
    account_id:{
      type: Number,
      default:null
    },
  	steamInfo: {
  		type: Object,
  		default:{}
  	},
    level:{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'levels'
    },
    team:{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'teams'
    },
    dota_score:{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'rating_dota'  
    },
    balance:{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'balance'  
    },
    csgo_score:{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'rating_csgo'  
    },
    role:{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'roles'  
    },
    ban:{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'bans'  
    },
},{
  timestamps: true
})

userSchema.plugin(deepPopulate(mongoose))

const users = mongoose.model('user', userSchema)

export default users