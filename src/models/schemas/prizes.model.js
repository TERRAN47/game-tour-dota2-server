import mongoose from '../../connectors/mongo'
import deepPopulate from 'mongoose-deep-populate'

const prizeSchema = new mongoose.Schema({
  	poster: {
  		type: String,
  		default:''
  	},
    title:{
      type: String,
      default:''
    },
    price:{
      type: Number,
      default:0
    },
    admin:{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'user'
    }
})

prizeSchema.plugin(deepPopulate(mongoose))
const prizes = mongoose.model('prizes', prizeSchema)

export default prizes