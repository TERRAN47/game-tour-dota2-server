import mongoose from '../../connectors/mongo'
import deepPopulate from 'mongoose-deep-populate'

const teamSchema = new mongoose.Schema({
	leader_id: {
		type: mongoose.Schema.Types.ObjectId
	},
	team_name: {
		type: String
	},
	team_logo: {
		type: String
	},
	abreviation: {
    	type: String
	},
	activeTeam: [{
		type:mongoose.Schema.Types.ObjectId,
		ref: 'user',
		default:[]
	}],
	reservTeam: [{
		type:mongoose.Schema.Types.ObjectId,
		ref: 'user',
		default:[]
	}]
})

teamSchema.plugin(deepPopulate(mongoose))
const teams = mongoose.model('teams', teamSchema)

export default teams