import mongoose from '../../connectors/mongo'
import deepPopulate from 'mongoose-deep-populate'

const transactionSchema = new mongoose.Schema({
  user:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user' 
  },
  transaction_code:{
    type:String,
    default:''
  },
  status:{
    type:Boolean,
    default:false
  },
  amount:{
    type:String,
    default:''
  }
},{
  timestamps: true
})

transactionSchema.plugin(deepPopulate(mongoose))
const transactions = mongoose.model('transactions', transactionSchema)

export default transactions