import mongoose from '../../connectors/mongo'
import deepPopulate from 'mongoose-deep-populate'

const tournamentSchema = new mongoose.Schema({
  user_creator:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user' 
  },
  game:{
    type:String,
    default:'DOTA2'  //DOTA2 or CSGO
  },
  players:[{
    type:mongoose.Schema.Types.ObjectId,
    ref: 'user'
  }],
  teams:[{
    type:mongoose.Schema.Types.ObjectId,
    ref: 'teams',
    default: []
  }],
  count_players:{
    type:Number,
    default:0
  },
  title:{
    type:String,
    default:''
  },
  poster:{
    data: Buffer,
    contentType: String
  },
  poster_path:{
    type: String,
    default:''
  },
  prize:{
    type: String,
    default:''
  },
  contribution:{
    type: String,
    default:''
  },
  currency:{
    type: String,
    default:'dollar'
  },
  select_heros: {
    type: String,
    default:'SINGLE' // ALL PICK
  },
  type_game:{
    type: String,
    default:'TEAM_VS_TEAM' //PLAYER_VS_PLAYER  
  },
  rules:{
    type: String,
    default:''  
  },
  grid:{
    type: Array,
    default:[] 
  },
  minimal_range:{
    type: Number,
    default:1  
  },
  time:{
    type: String,
    default:''  
  },
  date:{
    type: String,
    default:''  
  },
  status:{
    type: String,
    default:'WAIT' //WAIT START END  CLOSED
  }
},{
  timestamps: true
})

tournamentSchema.plugin(deepPopulate(mongoose))
const tournaments = mongoose.model('tournaments', tournamentSchema)

export default tournaments