import mongoose from '../../connectors/mongo'


const dota_heros = new mongoose.Schema({
  dota_heros:{
    type: Array,
    default:[]
  },
},{
  timestamps: true
})


const dotaHeros = mongoose.model('dota_heros', dota_heros)

export default dotaHeros