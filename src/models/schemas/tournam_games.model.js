import mongoose from '../../connectors/mongo'
import deepPopulate from 'mongoose-deep-populate'

const tournamGame_schema = new mongoose.Schema({
  team0: [{
    type:mongoose.Schema.Types.ObjectId,
    ref: 'user'
  }],
  team1: [{
    type:mongoose.Schema.Types.ObjectId,
    ref: 'user'
  }],
  myTeam0: [{
    type:mongoose.Schema.Types.ObjectId,
    default:null,
    ref: 'teams'
  }],
  myTeam1: [{
    type:mongoose.Schema.Types.ObjectId,
    default:null,
    ref: 'teams'
  }], 
  tournament_id:{
    type: String,
    default:''
  },
  round:{
    type: Number,
    default:1
  },
  bot_id:{
    type:mongoose.Schema.Types.ObjectId,
    default:null,
    ref: 'bots'
  },
  type_duel:{
    type: String,
    default:'ONE_BY_ONE' // TWO_BY_TWO OR ONE_BY_ONE
  },
  status:{
    type: Boolean,
    default:true
  },
  match_id:{
    type: Number,
    default:null
  },
  lobby_id:{
    type: String,
    default:''
  },
  userInLobby0:{
    type: Boolean,
    default:false
  },
  userInLobby1:{
    type: Boolean,
    default:false
  },
  team_victory:{
    type: Number,
    default:-1
  },
  lobby_login:{
    type: JSON,
    default:{}
  },
  lobby_info:{
    type: JSON,
    default:{}
  },
  random_heros:{
    type: Array,
    default:[]    
  }
},{
  timestamps: true
})

tournamGame_schema.plugin(deepPopulate(mongoose))
const tournamGames = mongoose.model('tournam_games', tournamGame_schema)

export default tournamGames