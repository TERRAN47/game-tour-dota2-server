import mongoose from '../connectors/mongo'
import users from './schemas/users.model'
import ips from './schemas/ip.model'
import levels from './schemas/levels.model'
import csgoRating from './schemas/rating_csgo.model'
import dotaRating from './schemas/rating_dota.model'
import duelsDota from './schemas/duels_dota.model'
import dotaHeros from './schemas/dota_heros.model'
import roles from './schemas/roles.model'
import tournaments from './schemas/tournament.model'
import teams from './schemas/teams.model'
import resultGames from './schemas/result_games.model'
import transactions from './schemas/transaction.model'
import bans from './schemas/ban.model'
import boosts from './schemas/boosts.model'
import resultDotaGames from './schemas/result_dota_games.model'
import bots from './schemas/bots.model'
import tournamGames from './schemas/tournam_games.model'
import friends from './schemas/friend.model'
import prizes from './schemas/prizes.model'
import prestige from './schemas/prestige.model'
import balance from './schemas/balance.model'
import notifyTeams from "./schemas/nitify_teams.model"
import transfers from "./schemas/transfers.model"

export {
	users,
	bots,
	ips,
	teams,
	dotaHeros,
	duelsDota,
	levels,
	dotaRating,
	csgoRating,
	roles,
	resultGames,
	tournaments,
	transactions,
	bans,
	resultDotaGames,
	boosts,
	tournamGames,
	friends,
	prizes,
	prestige,
	notifyTeams,
	balance,
	transfers
}